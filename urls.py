from django.conf import settings
from django.contrib import admin
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic.base import RedirectView

from oscar.views import handler500, handler404

from app import application

admin.autodiscover()

urlpatterns = patterns('',
                       (r'^admin/', include(admin.site.urls)),
                       (r'', include(application.urls)),
                       )

urlpatterns += patterns('',
                        url(r'^dich-anh-viet/', include('rosetta.urls')),
                        url(r'^ckeditor/', include('ckeditor_uploader.urls')),
                        url(r'^oauth/', include('social_django.urls', namespace='social')),
                        url(r'^oauth/', include('django.contrib.auth.urls', namespace='auth')),
                        url(r'^favicon.ico/$', RedirectView.as_view(
                            url='/static/binhphuoc/favicon.ico', permanent=True)
                            ),
                        url(r'^lien-he/', include('contact_form.urls')),
                        )
if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += patterns('',
                            url(r'^404$', handler404),
                            url(r'^500$', handler500),
                            )

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += patterns(
        '',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
