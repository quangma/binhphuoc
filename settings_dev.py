import os

import sys
# ===========================
# = Directory Declaractions =
# ===========================
CURRENT_DIR = os.path.dirname(__file__)
VENDOR_ROOT = os.path.join(CURRENT_DIR, 'vendor')
# ==============
# = PYTHONPATH =
# ==============
# if '/vendor' not in ' '.join(sys.path):
sys.path.append(VENDOR_ROOT)

# = END PYTHON PATH
# Import default settings for the project
from conf.default import *

# We use an environmental variable to indicate the, erm, environment
# This block writes all settings to the local namespace
module_path = os.environ.get('DJANGO_CONF', 'conf.dev')
try:
    module = __import__(module_path, globals(), locals(), ['*'])
except ImportError:
    import sys
    print "You need to create a file conf/dev.py to contain your local settings"
    sys.exit(1)

for k in dir(module):
    if not k.startswith("__"):
        locals()[k] = getattr(module, k)

# Keep version number here - this is generally overwritten as
# part of deployment to be the build name
VERSION = 'VnMall 1.1'