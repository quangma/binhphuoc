/**
 * Created by Tran Van on 9/10/2015.
 */
$(document).ready(function() {
    $("#width_tmp").html($('#search_select option:selected').text());
    var search_select = $('#search_select');
    search_select.width($("#width_tmp").width()+30);
    var input = $("#id_q");
    input.width($('#search_input_tmp').width() - 20 - $(search_select).width());

    search_select.change(function(){
        $("#width_tmp").html($('select option:selected').text());
        $(this).width($("#width_tmp").width()+30); // 30 : the size of the down arrow of the select box
         // 660px ~ col-md-8
        var input = $("#id_q");
        input.width($('#search_input_tmp').width() - 20 - $(this).width());
        input.selectionStart = input.selectionEnd = input.val().length;
        input.focus();
    });
});
