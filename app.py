# -*- coding:utf-8 -*-
from django.conf.urls import url, include, patterns

from oscar.app import Shop
from oscar.core.loading import get_class


class Application(Shop):
    name = None

    news_app = get_class('news.app', 'application')
    enterprise_app = get_class('enterprise.app', 'application')
    document_app = get_class('document.app', 'application')

    def get_urls(self):
        urls = super(Application, self).get_urls()
        # Đổi đường dẫn catalogue, search cho thân thiện
        urls[0] = url(r'^mua-sam/', include(self.catalogue_app.urls))
        urls[4] = url(r'^tim-kiem/', include(self.search_app.urls))
        urls += [
            url(r'^tin-tuc/', include(self.news_app.urls)),
            url(r'^doanh-nghiep/', include(self.enterprise_app.urls)),
            url(r'^van-ban-phap-luat/', include(self.document_app.urls)),
        ]
        return urls


application = Application()
