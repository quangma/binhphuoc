﻿# -*- coding: utf-8 -*-
import os

from django.utils.translation import ugettext_lazy as _

location = lambda x: os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', x)

DEBUG = True
# for THUMBNAIL debug
THUMBNAIL_DEBUG = True

ADMINS = (
    ('Tran Van', "vantxm@yahoo.co.uk"),
)

MANAGERS = (
    ('Tran Van', "vantxm@gmail.com"),
    (u'Trung tâm Khuyến Công và Tư vấn Phát triển Công nghiệp tỉnh Bình Phước', "ttkc.sct@binhphuoc.gov.vn"),
)

TIME_ZONE = 'Asia/Saigon'

LANGUAGE_CODE = 'vi-vn'

LANGUAGES = (
    ('vi', 'Vietnamese'),
)

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# vant add for language translation
LOCALE_PATHS = (location('locale', ), location('i18n/oscar'),)

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = location('public/media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = location('public/static/')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a  trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
# ADMIN_MEDIA_PREFIX = '/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    location('static/'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '7o%&amp;401wedtybce1tb9o5a5#$uyz@kmf4kq85xzfx^+(m5bz$5'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.request",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    'oscar.apps.search.context_processors.search_form',
    'apps.search.context_processors.home_search_form',
    'oscar.apps.promotions.context_processors.promotions',
    'oscar.apps.checkout.context_processors.checkout',
    'oscar.apps.customer.notifications.context_processors.notifications',
    'oscar.core.context_processors.metadata',
    'apps.promotions.context_processsors.metadata',

    'social_django.context_processors.backends',
    'social_django.context_processors.login_redirect',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'oscar.apps.basket.middleware.BasketMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'request.middleware.RequestMiddleware',

    'social_django.middleware.SocialAuthExceptionMiddleware',
)

ROOT_URLCONF = 'urls'

from oscar import OSCAR_MAIN_TEMPLATE_DIR

TEMPLATE_DIRS = (
    location('templates'),
    location('templates/oscar'),
    OSCAR_MAIN_TEMPLATE_DIR,
)
# Cookies
OSCAR_RECENTLY_VIEWED_PRODUCTS = 15
OSCAR_COOKIES_DELETE_ON_LOGOUT = []
OSCAR_RECENTLY_VIEWED_COOKIE_LIFETIME = 365 * 24 * 60 * 60

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.admin',
    'django.contrib.flatpages',
    'django.contrib.staticfiles',
    'compressor',
    'widget_tweaks',
    'easy_thumbnails',
    'ckeditor_uploader',
    'ckeditor',
    'filer',
    'mptt',
    'apps.news',
    'apps.enterprise',
    'cashondelivery',
    'apps.document',
    'apps.user',
    # django-request: statistics module
    'request',
    'social_django',
    'contact_form',
]

OVERRIDES_APP = ['apps.catalogue',
                 'apps.dashboard',
                 'apps.dashboard.catalogue',
                 'apps.dashboard.partners',
                 'apps.dashboard.users',
                 'apps.dashboard.promotions',
                 'apps.dashboard.pages',
                 'apps.promotions',
                 'apps.partner',
                 'apps.order',
                 'apps.address',
                 'apps.search',
                 'apps.customer',
                 'apps.basket',
                 'apps.checkout',
                 'apps.shipping',
                 ]

from oscar import get_core_apps

INSTALLED_APPS += get_core_apps(OVERRIDES_APP)

# STATICFILES_STORAGE = 'pipeline.storage.PipelineCachedStorage'
# This is set as in a HTML comment at the bottom of the page
HOSTNAME = 'N/A'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOG_ROOT = location('./logs/')

SESSION_ENGINE = "django.contrib.sessions.backends.cached_db"
SESSION_COOKIE_HTTPONLY = True

AUTHENTICATION_BACKENDS = (
    'oscar.apps.customer.auth_backends.EmailBackend',
    'django.contrib.auth.backends.ModelBackend',

    'social_core.backends.facebook.FacebookOAuth2',
    'social_core.backends.google.GoogleOAuth2',
    'social_core.backends.google.GoogleOpenId',
    'social_core.backends.open_id.OpenIdAuth',
)
# DATE_FORMAT = 'd/m/y'
# DATE_FORMAT = 'yy-mm-dd'
DATETIME_FORMAT = 'dd-mm-YYYY H:i:s'
DATE_INPUT_FORMATS = ('%d-%m-%Y',)

USE_GOOGLE_ANALYTICS = False

# Auth social
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/accounts/login'
LOGOUT_URL = '/accounts/logout'
SOCIAL_AUTH_FACEBOOK_KEY = '211015469756901'  # App ID
SOCIAL_AUTH_FACEBOOK_SECRET = 'f6bccfb15d2235b809ae454f162dde57'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']
SOCIAL_AUTH_URL_NAMESPACE = 'social'
SOCIAL_AUTH_FACEBOOK_API_VERSION = '3.1'

# SOCIAL_AUTH_PIPELINE = (
#     'social.pipeline.social_auth.social_details',
#     'social.pipeline.social_auth.social_uid',
#     'social.pipeline.social_auth.auth_allowed',
#     'social.pipeline.social_auth.social_user',
#     'social.pipeline.user.get_username',
#     'social.pipeline.user.create_user',
#     'social.pipeline.social_auth.associate_user',
#     'social.pipeline.debug.debug',
#     'social.pipeline.social_auth.load_extra_data',
#     'social.pipeline.user.user_details',
#     'social.pipeline.debug.debug',
# )

SOCIAL_AUTH_GOOGLE_OAUTH2_IGNORE_DEFAULT_SCOPE = True
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = [
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/userinfo.profile'
]
# # Google+ SignIn (google-plus)
# SOCIAL_AUTH_GOOGLE_PLUS_IGNORE_DEFAULT_SCOPE = True
# SOCIAL_AUTH_GOOGLE_PLUS_SCOPE = [
#     'https://www.googleapis.com/auth/plus.login',
#     'https://www.googleapis.com/auth/userinfo.email',
#     'https://www.googleapis.com/auth/userinfo.profile'
# ]

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '975947022770-bdeguit134tsgn8bunel7rjnecg6dgnc.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'hCo3wRAWH_Z52LnAxDoxeEGk'

# For displaying version
DISPLAY_VERSION = False

# Disabled for local but enabled in real envs
COMPRESS_ENABLED = False
COMPRESS_OUTPUT_DIR = 'cache'
COMPRESS_CACHE_KEY_FUNCTION = 'compressor.cache.socket_cachekey'
COMPRESS_OFFLINE = True

AUTH_PROFILE_MODULE = 'user.Profile'

# Oscar settings
from oscar.defaults import *

OSCAR_ALLOW_ANON_CHECKOUT = True
OSCAR_INITIAL_ORDER_STATUS = u'Đợi giao hàng'
OSCAR_INITIAL_LINE_STATUS = u'Đợi giao hàng'

OSCAR_LINE_STATUS_PIPELINE = {
    u'Đợi giao hàng': (u'Đợi thanh toán', u'Đã thanh toán', u'Giao dịch thành công', u'Hủy',),
    u'Đợi thanh toán': (u'Đã thanh toán', u'Hủy', u'Giao dịch thành công'),
    u'Hủy': (u'Đợi giao hàng', u'Đợi thanh toán', u'Đã thanh toán', u'Giao dịch thành công',),
    u'Đã thanh toán': (u'Giao dịch thành công'),
    u'Giao dịch thành công': ()
}

OSCAR_SHOP_NAME = u'Sàn giao dịch nông sản tỉnh Bình Phước'
OSCAR_SHOP_TAGLINE = u'Đồng hành cùng nhà nông'

GOOGLE_ANALYTICS_ID = 'UA-XXXXX-Y'

# Vant add
# Currency
# Xem phan hien thi tien te tai day
# http://babel.pocoo.org/docs/api/numbers/
OSCAR_DEFAULT_CURRENCY = 'đ'
OSCAR_CURRENCY_LOCALE = 'vi_VN'
OSCAR_CURRENCY_FORMAT = u'#,### ¤¤'

# vant add for themes support
THEMES = (
    'binhphuoc',
)

# Remove tracker
OSCAR_TRACKING = False

# Search facets
OSCAR_SEARCH_FACETS = {
    'fields': OrderedDict([
        # The key for these dicts will be used when passing facet data
        # to the template. Same for the 'queries' dict below.
        ('category', {'name': _('Category'), 'field': 'category'}),
        ('rating', {'name': _('Rating'), 'field': 'rating'}),
        # You can specify an 'options' element that will be passed to the
        # SearchQuerySet.facet() call.  It's hard to get 'missing' to work
        # correctly though as of Solr's hilarious syntax for selecting
        # items without a specific facet:
        # http://wiki.apache.org/solr/SimpleFacetParameters#facet.method
        # 'options': {'missing': 'true'}
    ]),
    'queries': OrderedDict([
        ('price_range',
         {
             'name': _('Price range'),
             'field': 'price',
             'queries': [
                 # This is a list of (name, query) tuples where the name will
                 # be displayed on the front-end.
                 (_('0 to 100000'), u'[0 TO 100000]'),
                 (_('100000 to 300000'), u'[100000 TO 300000]'),
                 (_('300000 to 1000000'), u'[300000 TO 1000000]'),
                 (_('1000000+'), u'[1000000 TO *]'),
             ]
         }),
    ]),
}

# Menu structure of the dashboard navigation
OSCAR_DASHBOARD_NAVIGATION = [
    {
        'label': _('Dashboard'),
        'icon': 'icon-th-list',
        'url_name': 'dashboard:index',
    },
    {
        'label': _(u'Quản lý bán hàng'),
        'icon': 'icon-shopping-cart',
        'children': [
            {
                'label': _('Orders'),
                'url_name': 'dashboard:order-list',
            },
            {
                'label': _('Statistics'),
                'url_name': 'dashboard:order-stats',
            },
            {
                'label': _(u'Doanh nghiệp'),
                'url_name': 'dashboard:partner-list',
            },
            {
                'label': _(u'Tài khoản doanh nghiệp'),
                'url_name': 'dashboard:partner-user-list',
            },
            # The shipping method dashboard is disabled by default as it might
            # be confusing. Weight-based shipping methods aren't hooked into
            # the shipping repository by default (as it would make
            # customising the repository slightly more difficult).
            # {
            # 'label': _('Shipping charges'),
            #     'url_name': 'dashboard:shipping-method-list',
            # },
        ]
    },

    {
        'label': _('Catalogue'),
        'icon': 'icon-sitemap',
        'children': [
            {
                'label': _('Products'),
                'url_name': 'dashboard:catalogue-product-list',
            },
            {
                'label': _('Product Types'),
                'url_name': 'dashboard:catalogue-class-list',
            },
            {
                'label': _('Categories'),
                'url_name': 'dashboard:catalogue-category-list',
            },
        ]
    },
    {
        'label': _('Customers'),
        'icon': 'icon-group',
        'children': [
            {
                'label': _('Customers'),
                'url_name': 'dashboard:users-index',
            },
            {
                'label': _('Stock alert requests'),
                'url_name': 'dashboard:user-alert-list',
            },
        ]
    },
    {
        'label': _('Content'),
        'icon': 'icon-folder-close',
        'children': [
            {
                'label': _('Pages'),
                'url_name': 'dashboard:page-list',
            },
            {
                'label': _(u'Tin tức'),
                'url_name': 'dashboard:news-item-list',
            },
            {
                'label': _(u"Văn bản pháp luật"),
                'url_name': 'dashboard:document-item-list',
            },
            {
                'label': _(u'Danh mục tin'),
                'url_name': 'dashboard:news-category-list',
            },
            {
                'label': _(u'Quản lý banner'),
                'url_name': 'dashboard:banner-list',
            },
            {
                'label': _(u'Quản lý video'),
                'url_name': 'dashboard:video-list',
            },
            # {
            #     'label': _('Email templates'),
            #     'url_name': 'dashboard:comms-list',
            # },
            # {
            #     'label': _('Reviews'),
            #     'url_name': 'dashboard:reviews-list',
            # },
        ]
    },
    {
        'label': _('Reports'),
        'icon': 'icon-bar-chart',
        'url_name': 'dashboard:reports-index',
    },
]

INSTALLED_APPS += ['rosetta', 'django_extensions', ]
ROSETTA_STORAGE_CLASS = 'rosetta.storage.CacheRosettaStorage'

# THUMBNAIL_COLORSPACE = None
THUMBNAIL_PRESERVE_FORMAT = True
OSCAR_CSV_INCLUDE_BOM = True

CKEDITOR_UPLOAD_PATH = "uploads/"

# contact form
CONTACT_UPLOAD_FILE_TYPES = ['.png', '.jpg', ]
# 2.5MB - 2621440
# 5MB - 5242880
# 10MB - 10485760
# 20MB - 20971520
# 50MB - 5242880
# 100MB 104857600
# 250MB - 214958080
# 500MB - 429916160
CONTACT_UPLOAD_FILE_MAX_SIZE = "5242880"
DEFAULT_FROM_EMAIL = 'emarketplacevnn@gmail.com'

# debug_toolbar settings
if DEBUG:
    INTERNAL_IPS = ('127.0.0.1', '10.10.10.1')
    MIDDLEWARE_CLASSES += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    )

    INSTALLED_APPS += (
        'debug_toolbar',
    )

    DEBUG_TOOLBAR_CONFIG = {
        'INTERCEPT_REDIRECTS': False,
    }

    RENDER_PANELS = True


    def show_toolbar(request):
        return True


    SHOW_TOOLBAR_CALLBACK = show_toolbar

    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
            'LOCATION': location('cache'),
        }
    }
