# -*- coding: utf-8 -*-
from django import forms
from django.forms.widgets import Input
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist
from oscar.apps.search.forms import SearchForm as CoreSearchForm

from oscar.core.loading import get_model


Category = get_model('catalogue', 'Category')


class SearchInput(Input):
    """
    Defining a search type widget

    This is an HTML5 thing and works nicely with Safari, other browsers default
    back to using the default "text" type
    """
    input_type = 'search'


class SearchForm(CoreSearchForm):
    category_id = forms.CharField(required=False)

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        self.fields['q'].widget = SearchInput({
            "placeholder": _(u'Tìm kiếm sản phẩm'),
            "tabindex": "1",
            #"class": "form-control form-control-sm"
        })

    def search(self):
        sqs = super(SearchForm, self).search()
        try:
            category = Category.objects.get(pk=self.cleaned_data['category_id'])
            if category:
                categories = category.get_descendants_and_self()
                pattern = ' OR '.join([
                    '"%s"' % c.id for c in categories])
                sqs = sqs.narrow('category_id_exact:(%s)' % pattern)
        except (ObjectDoesNotExist, AttributeError) as e:
            pass

        return sqs


class HomeSearchForm(CoreSearchForm):
    category_id = forms.CharField(required=False)

    def __init__(self, *args, **kwargs):
        super(HomeSearchForm, self).__init__(*args, **kwargs)
        self.fields['q'].widget = SearchInput({
            "tabindex": "1",
            "class": "form-control w-100"
        })

    def search(self):
        sqs = super(HomeSearchForm, self).search()
        try:
            category = Category.objects.get(pk=self.cleaned_data['category_id'])
            if category:
                categories = category.get_descendants_and_self()
                pattern = ' OR '.join([
                    '"%s"' % c.id for c in categories])
                sqs = sqs.narrow('category_id_exact:(%s)' % pattern)
        except (ObjectDoesNotExist, AttributeError) as e:
            pass

        return sqs