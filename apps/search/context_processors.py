from oscar.core.loading import get_class

HomeSearchForm = get_class('search.forms', 'HomeSearchForm')


def home_search_form(request):
    """
    Ensure that the search form is available site wide
    """
    return {'home_search_form': HomeSearchForm(request.GET)}
