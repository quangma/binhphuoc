# -*- coding: utf-8 -*-
__author__ = 'vant'

from django.conf import settings
from oscar.apps.search.views import FacetedSearchView as CoreFacetedSearchView

if settings.THEMES:
    THEME = settings.THEMES[0]


class FacetedSearchView(CoreFacetedSearchView):
    """
    A modified version of Haystack's FacetedSearchView

    Note that facets are configured when the ``SearchQuerySet`` is initialised.
    This takes place in the search application class.

    See http://django-haystack.readthedocs.org/en/v2.1.0/views_and_forms.html#facetedsearchform # noqa
    """
    template = "%s/search/results.html" % THEME
