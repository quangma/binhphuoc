# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('promotions', '0004_auto_20150702_1538'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='active',
            field=models.BooleanField(default=True),
        ),
    ]
