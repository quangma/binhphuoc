# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('promotions', '0008_auto_20181224_1035'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='banner_in_type',
            field=models.CharField(default=b'OBL', max_length=5, choices=[(b'IBP', 'Banner m\u1ee5c qu\u1ea3ng c\xe1o'), (b'IBC', 'Banner trong n\u1ed9i dung trang'), (b'IBB', 'Banner cu\u1ed1i n\u1ed9i dung trang'), (b'IBT', 'Banner \u0111\u1ea7u n\u1ed9i dung trang '), (b'OBL', 'Banner ngo\xe0i b\xean tr\xe1i'), (b'OBR', 'Banner ngo\xe0i b\xean ph\u1ea3i'), (b'OBT', 'Banner tr\xean c\xf9ng'), (b'POPUP', 'Banner POP UP'), (b'SLIDE', 'Slide ch\xednh trang ch\u1ee7'), (b'IBS', 'Banner c\u1ea1nh slide ch\xednh'), (b'IBF', 'Banner top b\xe1n ch\u1ea1y'), (b'CBH', 'Slide danh m\u1ee5c s\u1ea3n ph\u1ea9m')]),
        ),
        migrations.AlterField(
            model_name='banner',
            name='page',
            field=models.CharField(default=b'HOME', max_length=20, choices=[(b'HOME', 'Trang ch\u1ee7'), (b'CAT_LV', 'Trang danh m\u1ee5c'), (b'CAT_LV2', 'Trang danh m\u1ee5c c\u1ea5p 2'), (b'AUTHEN', 'Trang \u0111ang nh\u1eadp/\u0111ang k\xfd'), (b'PRODUCT', 'Trang s\u1ea3n ph\u1ea9m'), (b'NEWS', 'Trang tin t\u1ee9c')]),
        ),
    ]
