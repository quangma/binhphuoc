# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('promotions', '0006_auto_20150925_1608'),
    ]

    operations = [
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Ti\xeau \u0111\u1ec1')),
                ('video', models.FileField(upload_to=b'', null=True, verbose_name='T\u1ec7p video', blank=True)),
                ('url', models.URLField(help_text='\u0110\u1ecba ch\u1ec9 URL tr\xean Youtube', null=True, verbose_name='\u0110\u1ecba ch\u1ec9 URL', blank=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Ng\xe0y t\u1ea1o')),
                ('date_updated', models.DateTimeField(auto_now=True, verbose_name='Ng\xe0y c\u1eadp nh\u1eadt', db_index=True)),
            ],
        ),
    ]
