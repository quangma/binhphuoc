# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('promotions', '0002_auto_20150604_1450'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdClick',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('click_date', models.DateTimeField(auto_now_add=True, verbose_name='When')),
                ('source_ip', models.GenericIPAddressField(null=True, verbose_name='Who', blank=True)),
            ],
            options={
                'verbose_name': 'Ad Click',
                'verbose_name_plural': 'Ad Clicks',
            },
        ),
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('banner_in_type', models.CharField(default=b'OBL', max_length=5, choices=[(b'HTL', 'Tab b\xean tr\xe1i trang'), (b'HTR', 'Tab b\xean ph\u1ea3i trang'), (b'OBL', 'Banner ngo\xe0i b\xean tr\xe1i'), (b'OBR', 'Banner ngo\xe0i b\xean ph\u1ea3i'), (b'POPUP', 'Banner POP UP')])),
                ('banner_name', models.CharField(default=b'', max_length=255, verbose_name='T\xean banner')),
                ('title', models.CharField(max_length=255, verbose_name='Title cho seo', blank=True)),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('banner', models.ImageField(upload_to=b'images/banner/', verbose_name='\u1ea2nh banner')),
                ('group_order', models.PositiveIntegerField(verbose_name='Thu\u1ed9c nh\xf3m th\u1ee9 i trong trang', choices=[(0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10), (11, 11), (12, 12), (13, 13), (14, 14)])),
                ('local_order', models.PositiveIntegerField(default=0, verbose_name='Th\u1ee9 t\u1ef1 hi\u1ec3n trong nh\xf3m', choices=[(0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10), (11, 11), (12, 12), (13, 13), (14, 14)])),
                ('page', models.CharField(default=b'HOME', max_length=20, choices=[(b'HOME', 'Trang ch\u1ee7'), (b'CAT_LV', 'Trang danh m\u1ee5c'), (b'CAT_LV2', 'Trang danh m\u1ee5c c\u1ea5p 2'), (b'PRODUCT', 'Trang s\u1ea3n ph\u1ea9m')])),
                ('link', models.URLField(blank=True)),
                ('active', models.BooleanField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='adclick',
            name='banner',
            field=models.ForeignKey(to='promotions.Banner'),
        ),
    ]
