# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('promotions', '0007_video'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='banner_in_type',
            field=models.CharField(default=b'OBL', max_length=5, choices=[(b'IBL', 'Banner trong b\xean tr\xe1i trang'), (b'IBR', 'Banner trong b\xean ph\u1ea3i trang'), (b'OBL', 'Banner ngo\xe0i b\xean tr\xe1i'), (b'OBR', 'Banner ngo\xe0i b\xean ph\u1ea3i'), (b'IBB', 'Banner trong \u1edf d\u01b0\u1edbi'), (b'IBT', 'Banner trong \u1edf tr\xean'), (b'POPUP', 'Banner POP UP'), (b'SLIDE', 'Banner Slide show'), (b'CBH', 'Banner Slide catalogy home')]),
        ),
        migrations.AlterField(
            model_name='banner',
            name='page',
            field=models.CharField(default=b'HOME', max_length=20, choices=[(b'HOME', 'Trang ch\u1ee7'), (b'CAT_LV', 'Trang danh m\u1ee5c'), (b'CAT_LV2', 'Trang danh m\u1ee5c c\u1ea5p 2'), (b'AUTHEN', 'Trang authen'), (b'PRODUCT', 'Trang s\u1ea3n ph\u1ea9m'), (b'NEWS', 'Trang tin t\u1ee9c')]),
        ),
    ]
