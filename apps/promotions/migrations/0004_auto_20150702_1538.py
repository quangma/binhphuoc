# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('promotions', '0003_auto_20150701_1426'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='banner_in_type',
            field=models.CharField(default=b'OBL', max_length=5, choices=[(b'IBL', 'Banner trong b\xean tr\xe1i trang'), (b'IBR', 'Banner trong b\xean ph\u1ea3i trang'), (b'OBL', 'Banner ngo\xe0i b\xean tr\xe1i'), (b'OBR', 'Banner ngo\xe0i b\xean ph\u1ea3i'), (b'IBB', 'Banner trong \u1edf d\u01b0\u1edbi'), (b'IBT', 'Banner trong \u1edf tr\xean'), (b'POPUP', 'Banner POP UP')]),
        ),
        migrations.AlterField(
            model_name='banner',
            name='title',
            field=models.CharField(max_length=255, verbose_name='Ti\xeau \u0111\u1ec1 cho seo', blank=True),
        ),
    ]
