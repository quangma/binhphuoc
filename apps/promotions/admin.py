# -*- coding: utf-8 -*-
from django.contrib import admin

from oscar.core.loading import get_model

Banner = get_model('promotions', 'Banner')


class BannerAdmin(admin.ModelAdmin):
    model = Banner

admin.site.register(Banner, BannerAdmin)

from oscar.apps.promotions.admin import *  # noqa
