# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User


class Banner(models.Model):
    """

    """
    TYPE_BANNER = (
        ('IBP', _(u'Banner mục quảng cáo')),
        ('IBC', _(u'Banner trong nội dung trang')),
        ('IBB', _(u'Banner cuối nội dung trang')),
        ('IBT', _(u'Banner đầu nội dung trang ')),
        ('OBL', _(u'Banner ngoài bên trái')),
        ('OBR', _(u'Banner ngoài bên phải')),
        ('OBT', _(u'Banner trên cùng')),
        ('POPUP', _(u'Banner POP UP')),
        ('SLIDE', _(u'Slide chính trang chủ')),
        ('IBS', _(u'Banner cạnh slide chính')),
        ('IBF', _(u'Banner top bán chạy')),
        ('CBH', _(u'Slide danh mục sản phẩm')),
    )
    DISPLAY_IN_PAGE = (
        ('HOME', _(u"Trang chủ")),
        ('CAT_LV', _(u"Trang danh mục")),
        ('CAT_LV2', _(u"Trang danh mục cấp 2")),
        ('AUTHEN', _(u"Trang đang nhập/đang ký")),
        ('PRODUCT', _(u"Trang sản phẩm")),
        ('NEWS', _(u"Trang tin tức")),
    )
    CHOICES = [(i, i) for i in range(15)]
    DEFAULT = 'OBL'
    PAGE_DEFAULT = 'HOME'
    banner_in_type = models.CharField(max_length=5, choices=TYPE_BANNER, default=DEFAULT)
    banner_name = models.CharField(verbose_name=_(u'Tên banner'), max_length=255, default='')
    title = models.CharField(verbose_name=_(u'Tiêu đề cho seo'), max_length=255, blank=True)
    description = models.TextField(verbose_name=_(u'Description'), blank=True)
    banner = models.ImageField(verbose_name=_(u'Ảnh banner'), upload_to="images/banner/")
    group_order = models.PositiveIntegerField(choices=CHOICES,
                                              verbose_name=_(u'Thuộc nhóm thứ i trong trang'))
    local_order = models.PositiveIntegerField(choices=CHOICES, verbose_name=_(u'Thứ tự hiển trong nhóm'),
                                              default=0)
    page = models.CharField(max_length=20, choices=DISPLAY_IN_PAGE, default=PAGE_DEFAULT)
    link = models.URLField(blank=True)
    active = models.BooleanField(default=True)
    user = models.ForeignKey(User)

    @property
    def get_display_in_page(self):
        return self.DISPLAY_IN_PAGE[self.page]

    @property
    def get_clicks_count(self):
        return AdClick.objects.filter(banner=self.id).count()

    def __unicode__(self):
        return self.title


class AdClick(models.Model):
    """ The AdClick model will record every click that a add gets"""
    click_date = models.DateTimeField(
        verbose_name=_(u'When'), auto_now_add=True)
    source_ip = models.GenericIPAddressField(
        verbose_name=_(u'Who'), null=True, blank=True)
    banner = models.ForeignKey(Banner)

    class Meta:
        verbose_name = _('Ad Click')
        verbose_name_plural = _('Ad Clicks')


class Video(models.Model):
    """

    """
    title = models.CharField(verbose_name=_(u"Tiêu đề"), max_length=255)
    video = models.FileField(verbose_name=_(u"Tệp video"), blank=True, null=True)
    url = models.URLField(verbose_name=_(u"Địa chỉ URL"),
                          help_text=_(u"Địa chỉ URL trên Youtube"),
                          blank=True, null=True)
    date_created = models.DateTimeField(_(u"Ngày tạo"), auto_now_add=True)
    date_updated = models.DateTimeField(
        _(u"Ngày cập nhật"), auto_now=True, db_index=True)


from oscar.apps.promotions.models import *
