# -*- coding: utf-8 -*-
__author__ = 'vant'

from django.conf.urls import url
from oscar.core.loading import get_class
from oscar.apps.promotions.app import PromotionsApplication as CorePromotionsApplication


class PromotionsApplication(CorePromotionsApplication):
    home_view = get_class('promotions.views', 'HomeView')
    category_view = get_class('promotions.views', 'CategoryView')

    def get_urls(self):
        urls = super(PromotionsApplication, self).get_urls()
        urls += [url(r'^danh-muc/$', self.category_view.as_view(), name='category'), ]
        return self.post_process_urls(urls)


application = PromotionsApplication()