# -*- coding: utf-8 -*-

__author__ = 'vant'
from django.conf import settings

if settings.THEMES:
    THEME = settings.THEMES[0]


def metadata(request):
    """

    :param request:
    :return:
    """
    return {'THEME': THEME}