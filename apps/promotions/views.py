# -*- coding: utf-8 -*-
import random

from django.conf import settings
from django.views import generic

from haystack.query import SearchQuerySet

from oscar.apps.promotions.views import HomeView as CoreHomeView
from oscar.core.loading import get_model

from apps.promotions.settings import HOME_NEWS_CATEGORIES
from .settings import HOME_CATEGORIES, FEATURED_CATEGORY

Category = get_model('catalogue', 'Category')

if settings.THEMES:
    THEME = settings.THEMES[0]

# THEME = 'ecom2'
Product = get_model('catalogue', 'Product')
News = get_model('news', 'News')
NewsCategory = get_model('news', 'NewsCategory')
Banner = get_model('promotions', 'Banner')
Video = get_model('promotions', 'Video')
Partner = get_model('partner', 'Partner')
ProductRecord = get_model('analytics', 'ProductRecord')


class HomeView(CoreHomeView):
    template_name = '%s/home.html' % THEME
    model_white_list = [Product]
    cat_num_products = 5
    num_products = 20
    num_news = 5
    num_enterprises = 20

    def get_search_queryset(self, categories):
        sqs = SearchQuerySet()
        sqs = sqs.models(*self.model_white_list)
        if categories:
            # We use 'narrow' API to ensure Solr's 'fq' filtering is used as
            # opposed to filtering using 'q'.
            pattern = ' OR '.join([
                '"%s"' % c.name for c in categories])
            sqs = sqs.narrow('category_exact:(%s)' % pattern)
            # ordering ~ ?
            if len(sqs) > self.cat_num_products:
                sqs = random.sample(sqs, self.cat_num_products)

        return sqs

    def get_most_purchases_product(self):
        most_purchase_products = ProductRecord.objects.all().order_by('-num_purchases', '-num_views')[:2]
        return [p.product for p in most_purchase_products]

    def get_home_categories(self):
        """
        """
        home_categories = list()
        for slug, category_id, name, group_banner_id in HOME_CATEGORIES:
            try:
                category = Category.objects.get(id=category_id)
            except Category.DoesNotExist:
                category = None
            if category:
                category.descendants = category.get_descendants()
                # todo: chuyển sang dùng solr search để tăng tốc độ, hiện tại đang lỗi phần lấy giá sản phẩm
                # todo: chưa hiểu tại sao danh mục cũng dùng search engine mà vẫn lấy được csdl quan hệ
                # categories = list(category.descendants) + [category, ]
                # random_product = self.get_search_queryset(categories).load_all()
                random_product = Product.get_recursive_home_product(cat_id=category_id,
                                                                    num_product=self.cat_num_products)
                category.random_product = random_product
                category.Banners = Banner.objects.filter(group_order=group_banner_id, banner_in_type='CBH')
                home_categories.append(category)
        return home_categories

    def get_home_news(self):
        """
        :return:
        """
        home_news = list()
        for slug, category_id, name in HOME_NEWS_CATEGORIES:
            try:
                category = NewsCategory.objects.get(id=category_id)
            except NewsCategory.DoesNotExist:
                category = None
            if category:
                category.descendants = category.get_descendants()
                latest_news = News.get_recursive_latest_news(cat_id=category_id, num_news=self.num_news)
                category.latest_news = latest_news
                home_news.append(category)
        return home_news

    def get_latest_news(self):
        """
        :return:
        """
        return News.get_recursive_latest_news(num_news=self.num_news)

    @staticmethod
    def get_featured_products():
        featured_products = list()
        slug, name = FEATURED_CATEGORY
        try:
            category = Category.objects.get(slug=slug)
        except Category.DoesNotExist:
            category = None
        if category:
            featured_products = Product.get_feature_product(cat_id=category.id, num_product=24)
        return featured_products, category

    @staticmethod
    def get_partner_list():
        partner_list = Partner.objects.all()
        return partner_list

    @staticmethod
    def get_banner_list(group_order, banner_in_type, display_in_page):
        banner_list = Banner.objects.filter(group_order=group_order, banner_in_type=banner_in_type, active=True,
                                            page=display_in_page).order_by('local_order')
        return banner_list

    def get_context_data(self, **kwargs):
        """
        :param kwargs:
        :return:
        """
        ctx = super(HomeView, self).get_context_data(**kwargs)
        # san pham hot
        ctx['best_selling_product'] = Product.get_best_selling_product(
            cat_id=None, num_product=self.num_products)
        # san pham noi bat
        # phần này chưa chỉnh sửa, do chưa biết quy tắc tạo sản phẩm nổi bật
        # @todo: cần có định nghĩa thế nào là sản phẩm nổi bật
        ctx['feature_product'] = Product.get_feature_product(
            cat_id=None, num_product=self.num_products)
        ctx['random_product'] = Product.get_random_product(cat_id=None, num_product=self.num_products)
        ctx['home_categories'] = self.get_home_categories()
        ctx['home_news'] = self.get_home_news()
        ctx['latest_news'] = self.get_latest_news()
        ctx['partner_list'] = self.get_partner_list()
        ctx['top_focus_banners'] = self.get_banner_list(group_order=0, banner_in_type='IBF', display_in_page='HOME')
        ctx['top_banners'] = self.get_banner_list(group_order=0, banner_in_type='OBT', display_in_page='HOME')
        ctx['slides'] = self.get_banner_list(group_order=0, banner_in_type='SLIDE', display_in_page='HOME')
        ctx['slide_banners'] = self.get_banner_list(group_order=0, banner_in_type='IBS', display_in_page='HOME')

        # Counting
        ctx['total_products'] = Product.objects.count()
        ctx['total_partners'] = Partner.objects.count()

        # Featured Product
        ctx['featured_products'], ctx['featured_category'] = self.get_featured_products()

        ctx['video'] = Video.objects.order_by('-date_updated').first()

        # Enterprise list
        ctx['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]
        ctx['most_buy_products'] = self.get_most_purchases_product()

        return ctx


class CategoryView(generic.TemplateView):
    template_name = '%s_html/menu_2.html' % THEME
