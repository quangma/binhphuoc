# -*- coding: utf-8 -*-

__author__ = 'Vant'
from django import template
from django.template import loader
from django.template import Context
from django.utils.safestring import mark_safe
from django.conf import settings

register = template.Library()
if settings.THEMES:
    THEME = settings.THEMES[0]


@register.filter()
def add_left_space(value, num_space=1):
    """

    :param value:
    :return:
    """
    nbsp = "&nbsp;" * num_space
    return mark_safe(nbsp + value)


@register.simple_tag(takes_context=True)
def paginator(context, adjacent_pages=2, template_name='dashboard/pagination.html'):
    """
    To be used in conjunction with the object_list generic view.

    Adds pagination context variables for use in displaying first, adjacent and
    last page links in addition to those created by the object_list generic
    view.
    - Nếu trang hiện tại nhỏ hơn 2 + adjacent_pages :
        Trang bắt đầu sẽ là 1
        Trang kết thúc sẽ là 6
    - Nếu không: trang bắt đầu sẽ là:
        Trang hiện tại - (adjacent_pages + 1)
        Trang kết thúc sẽ là: trang hiện tại + adjacent_pages

    """
    t = loader.get_template(template_name)
    template_path = '%s/search/partials/pagination.html' % THEME
    if template_name == template_path:
        context['page_obj'] = context['page']
    current_page = context['page_obj'].number
    start_page = end_page = current_page
    if current_page < 2 + adjacent_pages:
        start_page = 1
        end_page = 6
    else:
        start_page = current_page - (adjacent_pages + 1)
        end_page = current_page + adjacent_pages

    page_numbers = [n for n in range(start_page, end_page + 1) if 0 < n <= context['paginator'].num_pages]
    page_obj = context['page_obj']
    paginator = context['paginator']

    return t.render(Context({
        'page_obj': page_obj,
        'paginator': paginator,
        'page_numbers': page_numbers,
        'has_next': context['page_obj'].has_next,
        'has_previous': context['page_obj'].has_previous,
        'show_first': 1 not in page_numbers,
        'show_last': paginator.num_pages not in page_numbers,
        'request': context['request']
    }))


def get_parameters(parser, token):
    """
    {% get_parameters except_field %}
    """

    args = token.split_contents()
    if len(args) < 2:
        raise template.TemplateSyntaxError(
            "get_parameters tag takes at least 1 argument")
    return GetParametersNode(args[1].strip())


class GetParametersNode(template.Node):
    """
    Renders current get parameters except for the specified parameter
    """

    def __init__(self, field):
        self.field = field

    def render(self, context):
        request = context['request']
        getvars = request.GET.copy()

        if self.field in getvars:
            del getvars[self.field]

        if len(getvars.keys()) > 0:
            get_params = "%s&" % getvars.urlencode()
        else:
            get_params = ''

        return get_params


get_parameters = register.tag(get_parameters)