# -*- coding: utf-8 -*-

__author__ = 'vant'
from django import template
from oscar.core.loading import get_model

register = template.Library()
Banner = get_model("promotions", 'Banner')


@register.filter
def banner_promotion_tag(value, banner_in_type='IBR', display_in_page='HOME'):
    """
    Tag lấy về danh sách các banner trên trang display_in_page,
    lấy tại: banner_in_type,
     Thứ tự của nhóm: value
    TYPE_BANNER = (
        ('IBP', _(u'Banner mục quảng cáo')),
        ('IBC', _(u'Banner trong phần nội dung trang')),
        ('IBB', _(u'Banner cuối nội dung trang')),
        ('IBT', _(u'Banner đầu nội dung trang ')),
        ('OBL', _(u'Banner ngoài bên trái')),
        ('OBR', _(u'Banner ngoài bên phải')),
        ('OBT', _(u'Banner ngoài ở trên')),
        ('POPUP', _(u'Banner POP UP')),
        ('SLIDE', _(u'Slide chính trang chủ')),
        ('IBS', _(u'Banner cạnh slide chính')),
        ('CBH', _(u'Slide danh mục sản phẩm')),
    )
    DISPLAY_IN_PAGE = (
        ('HOME', _(u"Trang chủ")),
        ('CAT_LV', _(u"Trang danh mục")),
        ('CAT_LV2', _(u"Trang danh mục cấp 2")),
        ('AUTHEN', _(u"Trang đang nhập/đang ký")),
        ('PRODUCT', _(u"Trang sản phẩm")),
        ('NEWS', _(u"Trang tin tức")),
    )
    """
    banner = Banner.objects.filter(banner_in_type=banner_in_type, group_order=value, active=True,
                                   page=display_in_page).order_by('local_order')
    return banner
