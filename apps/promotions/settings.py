# -*- coding: utf-8 -*-
__author__ = 'Vant'
"""
    Phần cấu hình cho danh mục hiển thị trên trang chủ
    Khi muốn thêm một danh mục hiển thị mới, cần làm các bước sau:
      - Thêm vào HOME_CATEGORIES ('slug', category_id, 'ten danh muc', banner_cat_home_id)
"""
HOME_CATEGORIES = (
    ('thu-cong-my-nghe-qua-tang', 63, 'Thủ công mỹ nghệ', 1,),
    ('nong-lam-thuy-san', 1, 'Nông - Lâm - Thủy sản', 2,),
    ('det-may-thoi-trang-phu-kien', 9, 'Dệt may thời trang & Phụ kiện', 3,),
    ('noi-ngoai-that', 23, 'Nội ngoại thất', 4,),
    ('che-bien-thuc-pham-do-uong', 55, 'Chế biến thực phẩm, đồ uống', 5,),
    ('dich-vu-khac', 120, 'Dịch vụ khác', 6,),
)

HOME_NEWS_CATEGORIES = (
    ('thong-tin-thi-truong', 1, "Thông tin thị trường"),
)

# Lấy thông tin về slug cho linh hoạt
FEATURED_CATEGORY = ('san-pham-tieu-bieu', "Sản phẩm tiêu biểu")
BEST_SELLING_CATEGORY = ('san-pham-ban-chay', "Sản Phẩm Bán Chạy")
