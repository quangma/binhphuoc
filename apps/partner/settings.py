# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _

COMPANY_TYPES = (
        ('DNVNN', _(u"Doanh nghiệp có vốn nhà nước")),
        ('DNFDI', _(u"Doanh nghiệp FDI")),
        ('CTTNHH', _(u"Công ty TNHH")),
        ('CTCP', _(u"Công ty cổ phần")),
        ('DNTN', _(u"Doanh nghiệp tư nhân")),
        ('HTX', _(u"Hợp tác xã")),
        ('HSXKD', _(u"Hộ sản xuất kinh doanh")),
    )