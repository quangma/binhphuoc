# -*- coding: utf-8 -*-
from django.core.cache import cache
from django.core.urlresolvers import reverse

from django.db import models
from django.utils.translation import ugettext_lazy as _

from oscar.apps.partner.abstract_models import AbstractPartner

from apps.partner.settings import COMPANY_TYPES


class Partner(AbstractPartner):
    """
    Hồ sơ của đối tác
    """
    category = models.ManyToManyField('catalogue.Category', related_name="partners", verbose_name=_("Category"))
    type = models.CharField(verbose_name=_(u'Loại hình doanh nghiệp'), choices=COMPANY_TYPES, max_length=32, blank=True)
    logo = models.ImageField(verbose_name=_(u'Logo'), upload_to='partners', blank=True, null=True)
    banner = models.ImageField(verbose_name=_(u'Banner'), upload_to='partners', blank=True, null=True)
    about = models.TextField(verbose_name=_(u"About partner"), blank=True, null=True)
    active = models.BooleanField(verbose_name=_('Active'), db_index=True, default=True)

    def get_absolute_url(self):
        """
        Our URL scheme means we have to look up the category's ancestors. As
        that is a bit more expensive, we cache the generated URL. That is
        safe even for a stale cache, as the default implementation of
        ProductCategoryView does the lookup via primary key anyway. But if
        you change that logic, you'll have to reconsider the caching
        approach.
        """
        cache_key = 'PARTNER_URL_%s' % self.pk
        url = cache.get(cache_key)
        if not url:
            url = reverse(
                'enterprise:enterprise',
                kwargs={'enterprise_slug': self.code, 'pk': self.pk})
            cache.set(cache_key, url)
        return url

from oscar.apps.partner.models import *  # noqa
