# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0005_partner_banner'),
    ]

    operations = [
        migrations.AddField(
            model_name='partner',
            name='type',
            field=models.CharField(blank=True, max_length=32, verbose_name='Lo\u1ea1i h\xecnh doanh nghi\u1ec7p', choices=[(b'DNVNN', 'Doanh nghi\u1ec7p c\xf3 v\u1ed1n nh\xe0 n\u01b0\u1edbc'), (b'DNFDI', 'Doanh nghi\u1ec7p FDI'), (b'CTTNHH', 'C\xf4ng ty TNHH'), (b'CTCP', 'C\xf4ng ty c\u1ed5 ph\u1ea7n'), (b'DNTN', 'Doanh nghi\u1ec7p t\u01b0 nh\xe2n'), (b'HTX', 'H\u1ee3p t\xe1c x\xe3'), (b'HSXKD', 'H\u1ed9 s\u1ea3n xu\u1ea5t kinh doanh')]),
        ),
    ]
