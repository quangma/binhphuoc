# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0006_auto_20150703_1623'),
        ('address', '0002_city_district_ward'),
        ('partner', '0003_auto_20150604_1450'),
    ]

    operations = [
        migrations.AddField(
            model_name='partner',
            name='about',
            field=models.TextField(null=True, verbose_name='About partner', blank=True),
        ),
        migrations.AddField(
            model_name='partner',
            name='active',
            field=models.BooleanField(default=True, db_index=True, verbose_name='Active'),
        ),
        migrations.AddField(
            model_name='partner',
            name='category',
            field=models.ManyToManyField(related_name='partners', verbose_name='Category', to='catalogue.Category'),
        ),
        migrations.AddField(
            model_name='partner',
            name='logo',
            field=models.ImageField(upload_to=b'partners', null=True, verbose_name='Logo', blank=True),
        ),
        migrations.AddField(
            model_name='partneraddress',
            name='city',
            field=models.ForeignKey(verbose_name='T\u1ec9nh/Th\xe0nh ph\u1ed1', blank=True, to='address.City', null=True),
        ),
        migrations.AddField(
            model_name='partneraddress',
            name='district',
            field=models.ForeignKey(verbose_name='Qu\u1eadn/Huy\u1ec7n', blank=True, to='address.District', null=True),
        ),
        migrations.AddField(
            model_name='partneraddress',
            name='email',
            field=models.EmailField(max_length=254, null=True, verbose_name='Email', blank=True),
        ),
        migrations.AddField(
            model_name='partneraddress',
            name='fax_number',
            field=phonenumber_field.modelfields.PhoneNumberField(max_length=128, null=True, verbose_name='Fax', blank=True),
        ),
        migrations.AddField(
            model_name='partneraddress',
            name='home_phone',
            field=phonenumber_field.modelfields.PhoneNumberField(default=b'+84', max_length=128, null=True, verbose_name='\u0110i\u1ec7n tho\u1ea1i c\u1ed1 \u0111\u1ecbnh', blank=True),
        ),
        migrations.AddField(
            model_name='partneraddress',
            name='mobile_phone',
            field=phonenumber_field.modelfields.PhoneNumberField(default=b'+84', max_length=128, null=True, verbose_name='Di \u0111\u1ed9ng', blank=True),
        ),
        migrations.AddField(
            model_name='partneraddress',
            name='ward',
            field=models.ForeignKey(verbose_name='Ph\u01b0\u1eddng/X\xe3', blank=True, to='address.Ward', null=True),
        ),
        migrations.AddField(
            model_name='partneraddress',
            name='website',
            field=models.URLField(null=True, verbose_name='Website', blank=True),
        ),
    ]
