# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0004_auto_20150703_1623'),
    ]

    operations = [
        migrations.AddField(
            model_name='partner',
            name='banner',
            field=models.ImageField(upload_to=b'partners', null=True, verbose_name='Banner', blank=True),
        ),
    ]
