# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0006_partner_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partneraddress',
            name='fax_number',
            field=models.CharField(max_length=128, null=True, verbose_name='Fax', blank=True),
        ),
        migrations.AlterField(
            model_name='partneraddress',
            name='home_phone',
            field=models.CharField(default=b'+84', max_length=128, null=True, verbose_name='\u0110i\u1ec7n tho\u1ea1i c\u1ed1 \u0111\u1ecbnh', blank=True),
        ),
        migrations.AlterField(
            model_name='partneraddress',
            name='mobile_phone',
            field=models.CharField(default=b'+84', max_length=128, null=True, verbose_name='Di \u0111\u1ed9ng', blank=True),
        ),
    ]
