# -*- coding: utf-8 -*-
from django.conf import settings
from django import forms
from django.contrib.auth.models import User
from django.forms import Textarea
from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.forms import AuthenticationForm as CoreAuthenticationForm
from oscar.apps.customer.forms import EmailUserCreationForm as coreEmailUserCreationForm
from oscar.core.loading import get_model

from oscar.apps.customer.utils import normalise_email
from oscar.core.validators import password_validators
from django.utils.http import is_safe_url

Partner = get_model('partner', 'Partner')


class AuthenticationForm(CoreAuthenticationForm):
    username = forms.CharField(required=True,
                               widget=forms.TextInput(attrs={'class': 'form-control',
                                                             'placeholder': _(u'Địa chỉ email hoặc tên đăng nhập')}))
    password = forms.CharField(label=_("Password"), required=True,
                               widget=forms.PasswordInput(attrs={'class': 'form-control',
                                                                 'placeholder': _(u'Mật khẩu')}))


class EmailUserCreationForm(coreEmailUserCreationForm):
    def save(self, commit=True):
        """
        Tự động tạo username theo email + một số tự nhiên
        :param commit:
        :return:
        """
        user = super(EmailUserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        check = True
        email = self.cleaned_data['email'].lower()
        found = email.find('@')
        if found != -1:
            count = 0
            username = email[:found]
            while check:
                try:
                    user_check = User.objects.get(username=username)
                    if user_check:
                        count += 1
                        username = email[:found] + str(count)
                except User.DoesNotExist:
                    check = False
                    if commit:
                        user.username = username
                        user.save()
        return user


class PartnerCreationForm(forms.ModelForm):
    class Meta:
        model = Partner
        fields = ('name', 'type',
                  'category', 'logo',
                  'banner', 'about')
        widgets = {
            'about': Textarea(attrs={'cols': 56, 'rows': 10}),
        }


class UserCreationForm(forms.ModelForm):
    email = forms.EmailField(label=_('Email address'))
    password1 = forms.CharField(
        label=_('Password'), widget=forms.PasswordInput,
        validators=password_validators)
    password2 = forms.CharField(
        label=_('Confirm password'), widget=forms.PasswordInput)
    redirect_url = forms.CharField(
        widget=forms.HiddenInput, required=False)

    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'
        )

    def __init__(self, host=None, *args, **kwargs):
        self.host = host
        super(UserCreationForm, self).__init__(*args, **kwargs)

    def clean_email(self):
        """
        Checks for existing users with the supplied email address.
        """
        email = normalise_email(self.cleaned_data['email'])
        if User._default_manager.filter(email__iexact=email).exists():
            raise forms.ValidationError(
                _("A user with that email address already exists"))
        return email

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1', '')
        password2 = self.cleaned_data.get('password2', '')
        if password1 != password2:
            raise forms.ValidationError(
                _("The two password fields didn't match."))
        return password2

    def clean_redirect_url(self):
        url = self.cleaned_data['redirect_url'].strip()
        if url and is_safe_url(url, self.host):
            return url
        return settings.LOGIN_REDIRECT_URL

    def save(self, commit=True):
        """
        Tự động tạo username theo email + một số tự nhiên
        :param commit:
        :return:
        """
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        check = True
        email = self.cleaned_data['email'].lower()
        found = email.find('@')
        if found != -1:
            count = 0
            username = email[:found]
            while check:
                try:
                    user_check = User.objects.get(username=username)
                    if user_check:
                        count += 1
                        username = email[:found] + str(count)
                except User.DoesNotExist:
                    check = False
                    if commit:
                        user.username = username
                        user.save()
        return user
