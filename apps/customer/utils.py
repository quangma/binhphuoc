# -*- coding: utf-8 -*-
import logging

import requests
from django.conf import settings
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.context import Context
from django.template.loader import get_template
from django.core.exceptions import ObjectDoesNotExist

from oscar.core.loading import get_model
from oscar.apps.customer.utils import Dispatcher as CoreDispatcher

CommunicationEvent = get_model('order', 'CommunicationEvent')
Email = get_model('customer', 'Email')


class Dispatcher(CoreDispatcher):
    """
    Viet lai de gui email den cho nguoi dung, nha cung cap
    """

    def __init__(self, logger=None, mail_connection=None):
        if not logger:
            logger = logging.getLogger(__name__)
        self.logger = logger
        self.mail_connection = mail_connection

    def send_email_messages(self, recipient, messages):
        """
        Plain email sending to the specified recipient
        """
        if hasattr(settings, 'OSCAR_FROM_EMAIL'):
            from_email = settings.OSCAR_FROM_EMAIL
        else:
            from_email = None

        # Determine whether we are sending a HTML version too
        if messages['html']:
            email = EmailMultiAlternatives(messages['subject'],
                                           messages['body'],
                                           from_email=from_email,
                                           to=[recipient])
            email.attach_alternative(messages['html'], "text/html")
        else:
            email = EmailMessage(messages['subject'],
                                 messages['body'],
                                 from_email=from_email,
                                 to=[recipient])
        self.logger.info("Sending email to %s" % recipient)
        if self.mail_connection:
            self.mail_connection.send_messages([email])
        else:
            email.send()
        return email

    def dispatch_order_messages(self, order, messages, event_type=None,
                                **kwargs):
        """
        Dispatch order-related messages to the customer
        """
        if order.is_anonymous:
            if 'email_address' in kwargs:
                self.send_email_messages(kwargs['email_address'], messages)
            elif order.guest_email:
                self.send_email_messages(order.guest_email, messages)
            else:
                return
        else:
            self.dispatch_user_order_messages(order, order.user, messages)

        # Create order communications event for audit
        if event_type is not None:
            CommunicationEvent._default_manager.create(
                order=order, event_type=event_type)

    def dispatch_user_order_messages(self, order, user, messages):
        """
        Send messages to a site user
        """
        if messages['subject'] and (messages['body'] or messages['html']):
            self.send_user_email_messages(user, messages)
        if messages['sms'] and settings.SEND_SMS:
            self.send_text_message(order, user, messages['sms'])

    def dispatch_lines_messages(self, lines, messages, event_type=None,
                                **kwargs):
        """
        Dispatch order-related messages to the partners
        @param event_type:
        @param messages:

        @param lines:
        """

        line_partners = [line.partner for line in lines]
        partner_emails = []
        for partner in line_partners:
            # Lay email tu tai khoan nguoi dung
            if partner.users:
                # Mot doanh nghiep co the co nhieu nguoi dung
                for user in partner.users.all():
                    if user.email not in partner_emails:
                        partner_emails.append(user.email)
            partner_address = partner.primary_address
            # Lay email tu thong tin doanh nghiep
            if partner_address:
                if partner_address.email and (partner_address.email not in partner_emails):
                    partner_emails.append(partner_address.email)
        for partner_email in partner_emails:
            try:
                self.send_email_messages(partner_email, messages)
            except:
                continue

    def _get_soap_xml(self, to_phone, message):

        template_name = 'oscar/customer/sms/sms_soap.xml'
        template = get_template(template_name)
        ctx = {
            'phone_number': to_phone,
            'message': message
        }
        return template.render(Context(ctx))

    def _send(self, to_phone, message):
        """
        Private method for send one message.

        :param SmsMessage message: SmsMessage class instance.
        :returns: True if message is sent else False
        :rtype: bool

        <?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Body>
                <sendsms_brandname xmlns="http://tempuri.org/">
                  <modile>0986931488</modile>
                  <noidung>Van kiem tra kha nang gui tin nhan cho Viettel</noidung>
                  <s_acc>vantxm2010</s_acc>
                  <s_pas>vantxm2010</s_pas>
                </sendsms_brandname>
              </soap:Body>
            </soap:Envelope>
        """
        data = self._get_soap_xml(to_phone, message)
        SMS_API_URL = 'http://sms.ecombinhphuoc.vn/Service_sendSMS.asmx'
        # todo: chuyen sang https cho an toan
        # SMS_API_URL = 'https://sms.ecombinhphuoc.vn/Service_sendSMS.asmx'
        headers = {'Content-Type': 'text/xml; charset=utf-8'}
        # todo: bỏ verify=False đi vì sẽ sinh lỗi
        # response = requests.post(SMS_API_URL, data, headers=headers, verify=False)
        response = requests.post(SMS_API_URL, data, headers=headers)
        self.logger.info(response.content)
        if response.status_code != 200:
            return False

        if 'success' not in response.content:
            return False
        # response = self._parse_response(response.content)
        if 'success' in response.content:
            return True

        return False

    def send_text_message(self, order, user, message):
        """
        shipping_address.phone_number
        @param user:
        @param message:
        @return:
        """
        SEND_SMS = settings.SEND_SMS
        if not SEND_SMS:
            return True

        phone_number = None
        try:
            phone_number = user.profile.phone_number
        except ObjectDoesNotExist:
            self.logger.info('Nguoi dung chua cap nhat thong tin profile')
        if not phone_number:
            phone_number = str(order.shipping_address.phone_number)
        if phone_number:
            phone_number = phone_number.replace(" ", "")
            phone_number = phone_number.replace("+84", "", 3)
            if phone_number[0] != '0':
                phone_number = "0{0}".format(phone_number)
            try:
                self._send(phone_number, message)
                self.logger.info("Da gui tin nhan cho so: %s", phone_number)
            except:
                print "Khong gui duoc tin nhan cho so: %s" % phone_number
                self.logger.warning("Khong gui duoc tin nhan cho so: %s", phone_number)
