# -*- coding: utf-8 -*-
from django.conf.urls import url

__author__ = 'vant'
from oscar.core.loading import get_class
from oscar.apps.customer.app import CustomerApplication as coreCustomerApplication


class CustomerApplication(coreCustomerApplication):
    name = 'customer'
    create_partner_view = get_class('customer.views', 'PartnerRegistrationView')
    partner_register_success_view = get_class('customer.views', 'PartnerRegisterSuccessView')

    def get_urls(self):
        urlpatterns = super(CustomerApplication, self).get_urls()
        urlpatterns += [
            url(r'^partner/$', self.create_partner_view.as_view(),
                name='create-partner'),
            url(r'^register-success/$', self.partner_register_success_view.as_view(),
                name='partner-register-success'),
        ]
        return self.post_process_urls(urlpatterns)


application = CustomerApplication()
