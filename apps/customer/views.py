# -*- coding: utf-8 -*-
from django.views.generic import TemplateView
# from django.contrib.auth import authenticate, login
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib.auth.models import Permission
from django.utils.translation import ugettext_lazy as _

from multi_form_view import MultiModelFormView

from django.conf import settings
from oscar.apps.customer.views import AccountAuthView as CoreAccountAuthView, \
    AccountRegistrationView as CoreAccountRegistrationView
from oscar.core.loading import get_model, get_class
from .forms import AuthenticationForm

THEME = settings.THEMES[0]

Partner = get_model('partner', 'Partner')
Product = get_model('catalogue', 'Product')
PartnerCreationForm = get_class('customer.forms', 'PartnerCreationForm')
UserCreationForm = get_class('customer.forms', 'EmailUserCreationForm')
Banner = get_model('promotions', 'Banner')


class AccountAuthView(CoreAccountAuthView):
    template_name = '%s/customer/login.html' % THEME
    num_products = 20

    @staticmethod
    def get_banner_list(group_order, banner_in_type, display_in_page):
        banner_list = Banner.objects.filter(group_order=group_order, banner_in_type=banner_in_type, active=True,
                                            page=display_in_page).order_by('local_order')
        return banner_list

    def get_context_data(self, *args, **kwargs):
        context = super(AccountAuthView, self).get_context_data(*args, **kwargs)
        context['best_selling_product'] = Product.get_best_selling_product(
            cat_id=None, num_product=self.num_products)
        context['login_form'] = AuthenticationForm(prefix=self.login_prefix)
        # Counting
        context['total_products'] = Product.objects.count()

        return context

    def get_login_success_url(self, form):
        redirect_url = form.cleaned_data['redirect_url']
        if redirect_url:
            return redirect_url

        # Redirect staff members to dashboard as that's the most likely place
        # they'll want to visit if they're logging in.
        is_enterprise = self.request.user.has_perm('partner.dashboard_access')
        if self.request.user.is_staff or is_enterprise:
            return reverse('dashboard:index')

        return settings.LOGIN_REDIRECT_URL


class AccountRegistrationView(CoreAccountRegistrationView):
    template_name = '%s/customer/registration.html' % THEME
    num_products = 20
    num_enterprises = 20

    @staticmethod
    def get_banner_list(group_order, banner_in_type, display_in_page):
        banner_list = Banner.objects.filter(group_order=group_order, banner_in_type=banner_in_type, active=True,
                                            page=display_in_page).order_by('local_order')
        return banner_list

    def get_context_data(self, *args, **kwargs):
        context = super(AccountRegistrationView, self).get_context_data(*args, **kwargs)
        context['best_selling_product'] = Product.get_best_selling_product(
            cat_id=None, num_product=self.num_products)
        context['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]

        return context


class PartnerRegistrationView(MultiModelFormView):
    form_classes = {
        'partner_form': PartnerCreationForm,
        'user_form': UserCreationForm,
    }
    partner_id = None
    template_name = '%s/customer/registration_partner_form.html' % THEME
    success_url = reverse_lazy('customer:partner-register-success')

    num_products = 20
    num_enterprises = 20

    def get_context_data(self, **kwargs):
        ctx = super(PartnerRegistrationView, self).get_context_data(**kwargs)
        ctx['best_selling_product'] = Product.get_best_selling_product(
            cat_id=None, num_product=self.num_products)
        # for upload image
        ctx['includes_files'] = True
        ctx['title'] = _('Khai báo thông tin doanh nghiệp')
        ctx['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]
        return ctx

    def link_user(self, user, partner):
        """
        Links a user to a partner, and adds the dashboard permission if needed.

        Returns False if the user was linked already; True otherwise.
        """
        if partner.users.filter(pk=user.pk).exists():
            return False
        partner.users.add(user)
        if not user.is_staff:
            dashboard_access_perm = Permission.objects.get(
                codename='dashboard_access',
                content_type__app_label='partner')
            user.user_permissions.add(dashboard_access_perm)
        return True

    def get_form_kwargs(self):
        kwargs = super(PartnerRegistrationView, self).get_form_kwargs()
        kwargs['partner_form']['prefix'] = 'partner'
        kwargs['user_form']['prefix'] = 'user'
        return kwargs

    def get_objects(self):
        self.partner_id = self.kwargs.get('partner_id', None)
        try:
            partner = Partner.objects.get(id=self.partner_id)
        except Partner.DoesNotExist:
            partner = None
        return {
            'partner_form': partner,
            'user_form': partner.user if partner else None,
        }

    def get_success_url(self):
        return reverse('customer:partner-register-success')

    def forms_valid(self, forms):
        user = forms['user_form'].save(commit=False)
        partner = forms['partner_form'].save(commit=False)
        username = forms['user_form'].cleaned_data['email']
        password = forms['user_form'].cleaned_data['password1']
        user.set_password(password)
        partner.save()
        user.save()
        self.link_user(user, partner)
        user.is_active = False
        user.save()
        # return redirect('customer:partner-register-success')
        # user = authenticate(username=username, password=password)
        # if user is not None:
        #     self.link_user(user, partner)
        #     print("authenticated")
        #     if user.is_active:
        #         login(self.request, user)
        #     return redirect('customer:profile-view')
        return super(PartnerRegistrationView, self).forms_valid(forms)


class PartnerRegisterSuccessView(TemplateView):
    template_name = '%s/customer/partner_register_sent.html' % THEME