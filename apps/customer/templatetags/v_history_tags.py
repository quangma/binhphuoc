from django import template
from django.conf import settings

from oscar.core.loading import get_model

from oscar.apps.customer import history

Site = get_model('sites', 'Site')

register = template.Library()


THEME = settings.THEMES[0]
recently_viewed_products_template = THEME + '/customer/history/recently_viewed_products.html'


@register.inclusion_tag(recently_viewed_products_template,
                        takes_context=True)
def v_recently_viewed_products(context):
    """
    Inclusion tag listing the most recently viewed products
    """
    request = context['request']
    products = history.get(request)
    return {'products': products,
            'request': request}
