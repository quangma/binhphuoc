# -*- coding: utf-8 -*-
import logging

from django.contrib.sites.models import Site, get_current_site
from django.core.urlresolvers import NoReverseMatch, reverse

from oscar.core.loading import get_model, get_class
from oscar.apps.checkout.mixins import OrderPlacementMixin as CoreOrderPlacementMixin

Dispatcher = get_class('customer.utils', 'Dispatcher')
CommunicationEventType = get_model('customer', 'CommunicationEventType')
# Standard logger for checkout events
logger = logging.getLogger('oscar.checkout')


class OrderPlacementMixin(CoreOrderPlacementMixin):
    """

    """

    def get_partner_message_context(self, order, lines):
        ctx = {
            'user': self.request.user,
            'order': order,
            'site': get_current_site(self.request),
            'lines': lines
        }

        if not self.request.user.is_authenticated():
            # Attempt to add the anon order status URL to the email template
            # ctx.
            try:
                path = reverse('customer:anon-order',
                               kwargs={'order_number': order.number,
                                       'hash': order.verification_hash()})
            except NoReverseMatch:
                # We don't care that much if we can't resolve the URL
                pass
            else:
                site = Site.objects.get_current()
                ctx['status_url'] = 'http://%s%s' % (site.domain, path)
        return ctx

    def get_partner_lines(self, order):
        """

        @param order:
        @return:
        """
        # Lay ra danh sach email cua cac doanh nghiep trong don hang
        # Moi don hang can lay ra thong tin: id doanh nghiep, cac lines tuong ung,
        lines = order.lines.all()
        # line_partners = {234: [line1, line2], 343: [line3],}
        line_partners = {}
        for line in lines:
            if line.partner.id in line_partners.keys():
                line_partners[line.partner.id].append(line)
            else:
                line_partners[line.partner.id] = [line, ]
        return line_partners

    def send_confirmation_message(self, order, code, **kwargs):
        ctx = self.get_message_context(order)
        try:
            event_type = CommunicationEventType.objects.get(code=code)
        except CommunicationEventType.DoesNotExist:
            # No event-type in database, attempt to find templates for this
            # type and render them immediately to get the messages.  Since we
            # have not CommunicationEventType to link to, we can't create a
            # CommunicationEvent instance.
            messages = CommunicationEventType.objects.get_and_render(code, ctx)
            event_type = None
        else:
            messages = event_type.get_messages(ctx)

        if messages and messages['body']:
            logger.info("Order #%s - sending %s messages", order.number, code)
            dispatcher = Dispatcher(logger)
            dispatcher.dispatch_order_messages(order, messages,
                                               event_type, **kwargs)
        else:
            logger.warning("Order #%s - no %s communication event type",
                           order.number, code)

        # send messages to partners
        code = 'ORDER_PARTNER'
        # Lay ra danh sach cac line trong don hang duoi dang dictionary
        line_partners = self.get_partner_lines(order)
        for line_partner in line_partners:
            # Voi moi partner lay ra thong tin hien thi khac nhau
            ctx = self.get_partner_message_context(order, line_partners[line_partner])
            try:
                event_type = CommunicationEventType.objects.get(code=code)
            except CommunicationEventType.DoesNotExist:
                # No event-type in database, attempt to find templates for this
                # type and render them immediately to get the messages.  Since we
                # have not CommunicationEventType to link to, we can't create a
                # CommunicationEvent instance.
                messages = CommunicationEventType.objects.get_and_render(code, ctx)
                event_type = None
            else:
                messages = event_type.get_messages(ctx)
            if messages and messages['body']:
                logger.info("Order #%s - sending %s messages for partner: #%s", order.number, code, line_partner)
                dispatcher = Dispatcher(logger)
                lines = line_partners[line_partner]
                dispatcher.dispatch_lines_messages(lines, messages, event_type, **kwargs)
            else:
                logger.warning("Order #%s - no %s communication event type",
                               order.number, code)
