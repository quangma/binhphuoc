# -*- coding: utf-8 -*-
import json
from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from oscar.core.loading import get_model, get_class, get_classes
from oscar.apps.checkout.views import IndexView as CoreIndexView
from oscar.apps.checkout.views import ShippingAddressView as CoreShippingAddressView
from oscar.apps.checkout.views import UserAddressUpdateView as CoreUserAddressUpdateView
from oscar.apps.checkout.views import ShippingMethodView as CoreShippingMethodView
from oscar.apps.checkout.views import ThankYouView as CoreThankYouView

from cashondelivery.views import PaymentDetailsView as CorePaymentDetailsView

current_theme = settings.THEMES[0]
Repository = get_class('shipping.repository', 'Repository')
ShippingAddressForm, GatewayForm \
    = get_classes('checkout.forms', ['ShippingAddressForm', 'GatewayForm'])

UserAddress = get_model('address', 'UserAddress')
Product = get_model('catalogue', 'Product')
Banner = get_model('promotions', 'Banner')
Partner = get_model('partner', 'Partner')


class IndexView(CoreIndexView):
    template_name = '%s/checkout/gateway.html' % (current_theme,)
    num_products = 20
    num_enterprises = 20

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated() or self.checkout_session.get_guest_email():
            return self.get_success_response()
        else:
            return HttpResponseRedirect(reverse('customer:login'))
        return super(IndexView, self).get(request, *args, **kwargs)

    @staticmethod
    def get_banner_list(group_order, banner_in_type, display_in_page):
        banner_list = Banner.objects.filter(group_order=group_order, banner_in_type=banner_in_type, active=True,
                                            page=display_in_page).order_by('local_order')
        return banner_list

    def get_context_data(self, **kwargs):
        """
        :param kwargs:
        :return:
        """

        ctx = super(IndexView, self).get_context_data(**kwargs)
        ctx['best_selling_product'] = Product.get_best_selling_product(
            cat_id=None, num_product=self.num_products)
        ctx['bottom_banners'] = self.get_banner_list(group_order=0, banner_in_type='IBB', display_in_page='HOME')
        ctx['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]
        return ctx


class PaymentDetailsView(CorePaymentDetailsView):
    template_name = '%s/checkout/payment_details.html' % current_theme
    template_name_preview = '%s/checkout/preview.html' % current_theme
    num_products = 20
    num_enterprises = 20

    @staticmethod
    def get_banner_list(group_order, banner_in_type, display_in_page):
        banner_list = Banner.objects.filter(group_order=group_order, banner_in_type=banner_in_type, active=True,
                                            page=display_in_page).order_by('local_order')
        return banner_list

    def get_context_data(self, **kwargs):
        """
        :param kwargs:
        :return:
        """

        ctx = super(PaymentDetailsView, self).get_context_data(**kwargs)
        ctx['best_selling_product'] = Product.get_best_selling_product(
            cat_id=None, num_product=self.num_products)
        ctx['bottom_banners'] = self.get_banner_list(group_order=0, banner_in_type='IBB', display_in_page='HOME')
        ctx['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]
        return ctx


class ShippingAddressView(CoreShippingAddressView):
    template_name = '%s/checkout/shipping_address.html' % current_theme
    num_products = 20
    num_enterprises = 20

    @staticmethod
    def get_banner_list(group_order, banner_in_type, display_in_page):
        banner_list = Banner.objects.filter(group_order=group_order, banner_in_type=banner_in_type, active=True,
                                            page=display_in_page).order_by('local_order')
        return banner_list

    def get_context_data(self, **kwargs):
        """
        :param kwargs:
        :return:
        """

        ctx = super(ShippingAddressView, self).get_context_data(**kwargs)
        ctx['best_selling_product'] = Product.get_best_selling_product(
            cat_id=None, num_product=self.num_products)
        ctx['bottom_banners'] = self.get_banner_list(group_order=0, banner_in_type='IBB', display_in_page='HOME')
        ctx['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]
        return ctx


class UserAddressUpdateView(CoreUserAddressUpdateView):
    template_name = 'checkout/user_address_form.html'


class ShippingMethodView(CoreShippingMethodView):
    template_name = '%s/checkout/shipping_methods.html' % current_theme
    num_products = 20
    num_enterprises = 20

    @staticmethod
    def get_banner_list(group_order, banner_in_type, display_in_page):
        banner_list = Banner.objects.filter(group_order=group_order, banner_in_type=banner_in_type, active=True,
                                            page=display_in_page).order_by('local_order')
        return banner_list

    def get_context_data(self, **kwargs):
        """
        :param kwargs:
        :return:
        """

        ctx = super(ShippingMethodView, self).get_context_data(**kwargs)
        ctx['best_selling_product'] = Product.get_best_selling_product(
            cat_id=None, num_product=self.num_products)
        ctx['bottom_banners'] = self.get_banner_list(group_order=0, banner_in_type='IBB', display_in_page='HOME')
        ctx['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]
        return ctx


class ThankYouView(CoreThankYouView):
    template_name = '%s/checkout/thank_you.html' % current_theme
    num_products = 20
    num_enterprises = 20

    @staticmethod
    def get_banner_list(group_order, banner_in_type, display_in_page):
        banner_list = Banner.objects.filter(group_order=group_order, banner_in_type=banner_in_type, active=True,
                                            page=display_in_page).order_by('local_order')
        return banner_list

    def get_context_data(self, **kwargs):
        """
        :param kwargs:
        :return:
        """
        ctx = super(ThankYouView, self).get_context_data(**kwargs)
        ctx['best_selling_product'] = Product.get_best_selling_product(
            cat_id=None, num_product=self.num_products)
        ctx['bottom_banners'] = self.get_banner_list(group_order=0, banner_in_type='IBB', display_in_page='HOME')
        ctx['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]
        return ctx
