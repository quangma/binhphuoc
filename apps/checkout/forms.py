# -*- coding: utf-8 -*-
from django import forms
from oscar.core.loading import get_model
from oscar.core.compat import get_user_model

User = get_user_model()
Country = get_model('address', 'Country')


class ShippingAddressForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ShippingAddressForm, self).__init__(*args, **kwargs)
        # self.fields['full_name'].widget.attrs['class'] = 'input_name'
        self.fields['full_name'].widget.attrs['placeholder'] = 'Nguyen Van A'
        # self.fields['line1'].widget.attrs['class'] = 'input_shippingaddress'
        self.fields['line1'].widget.attrs['placeholder'] = \
            u'Số nhà, tên đường, tên tòa nhà (nếu có), phường xã, quận (huyện), tỉnh (thành phố)'
        # self.fields['email'].widget.attrs['class'] = 'input_email'
        self.fields['email'].widget.attrs['placeholder'] = 'muahang@ecombinhphuoc.vn'
        # self.fields['phone_number'].widget.attrs['class'] = 'input_phonenumber'
        self.fields['phone_number'].widget.attrs['placeholder'] = '+8409xxxxxxxx'
        # self.fields['table_phone'].widget.attrs['class'] = 'input_tablenumber'
        # An mot so thong tin ve nuoc, ma vung trong form nhap lieu
        self.fields['country'].initial = 'VN'
        self.fields['country'].widget = forms.HiddenInput()
        self.fields['postcode'].initial = '100000'
        self.fields['postcode'].widget = forms.HiddenInput()

    class Meta:
        model = get_model('order', 'shippingaddress')
        fields = ('full_name', 'line1', 'email', 'phone_number', 'table_phone', 'postcode', 'country')
        exclude = ['title', 'first_name', 'last_name', 'line2', 'line3', 'line4',
                   'state', 'district', 'city', ]
