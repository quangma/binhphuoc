# -*- coding: utf-8 -*-
import json

from django.http import HttpResponse
from django.template import RequestContext
from django.template.loader import render_to_string
from oscar.core.loading import get_model

__author__ = 'vant'
from django.conf import settings
from django.core.urlresolvers import reverse

from oscar.apps.basket.views import BasketAddView as CoreBasketAddView
from oscar.apps.basket.views import BasketView as CoreBasketView

if settings.THEMES:
    THEME = settings.THEMES[0]

Product = get_model('catalogue', 'Product')
Banner = get_model('promotions', 'Banner')
Partner = get_model('partner', 'Partner')


class BasketAddView(CoreBasketAddView):
    """
     View mặc định không redirect về giỏ hàng (chưa rõ nguyên nhân). Vì vậy
     tùy biến lại view này để redirect về giỏ hàng.
    """
    def get_success_url(self):
        return reverse('basket:summary')


class BasketView(CoreBasketView):
    """
    Đang lỗi sau khi thao tác: cập nhật, ...
    Kiểm tra lại dữ liệu sau khi load ajax
    """
    template_name = '%s/basket/basket.html' % THEME
    num_enterprises = 20

    @staticmethod
    def get_banner_list(group_order, banner_in_type, display_in_page):
        banner_list = Banner.objects.filter(group_order=group_order, banner_in_type=banner_in_type, active=True,
                                            page=display_in_page).order_by('local_order')
        return banner_list

    def json_response(self, ctx, flash_messages):
        basket_content_template = '%s/basket/partials/basket_content.html' % THEME
        basket_html = render_to_string(
            basket_content_template,
            RequestContext(self.request, ctx))
        payload = {
            'content_html': basket_html,
            'messages': flash_messages.as_dict()}
        return HttpResponse(json.dumps(payload),
                            content_type="application/json")

    num_products = 20

    def get_context_data(self, **kwargs):
        """
        :param kwargs:
        :return:
        """
        ctx = super(BasketView, self).get_context_data(**kwargs)
        ctx['best_selling_product'] = Product.get_best_selling_product(
            cat_id=None, num_product=self.num_products)
        ctx['bottom_banners'] = self.get_banner_list(group_order=0, banner_in_type='IBB', display_in_page='HOME')
        ctx['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]
        return ctx
