# -*- coding: utf-8 -*-
__author__ = 'vant'
from oscar.apps.basket.app import BasketApplication as CoreBasketApplication


class BasketApplication(CoreBasketApplication):
    """
    """
    pass


application = BasketApplication()