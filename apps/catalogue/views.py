# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib import messages
from django.core.paginator import InvalidPage

from oscar.core.loading import get_model, get_class
from oscar.apps.catalogue.views import ProductCategoryView as CoreProductCategoryView
from oscar.apps.catalogue.views import CatalogueView as CoreCatalogueView
from oscar.apps.catalogue.views import ProductDetailView as CoreProductDetailView

from apps.promotions.settings import HOME_CATEGORIES
from .search_handlers import CustomProductSearchHandler

if settings.THEMES:
    THEME = settings.THEMES[0]

Product = get_model('catalogue', 'product')
Partner = get_model('partner', 'Partner')
Banner = get_model('promotions', 'Banner')
Category = get_model('catalogue', 'Category')
EnterpriseProductSearchHandler = get_class('enterprise.search_handlers', 'EnterpriseProductSearchHandler')


class ProductCategoryView(CoreProductCategoryView):
    """
    """
    num_products = 10
    num_enterprises = 20
    template_name = '%s/catalogue/category.html' % THEME

    def get_list_categories(self):
        """
        """
        list_categories = list()
        for slug, category_id, name, group_banner_id in HOME_CATEGORIES:
            try:
                category = Category.objects.get(id=category_id)
            except Category.DoesNotExist:
                category = None
            if category:
                category.descendants = category.get_descendants()
                list_categories.append(category)
        return list_categories

    def get_search_handler(self, *args, **kwargs):
        return CustomProductSearchHandler(*args, **kwargs)

    @staticmethod
    def get_banner_list(group_order, banner_in_type, display_in_page):
        banner_list = Banner.objects.filter(group_order=group_order, banner_in_type=banner_in_type, active=True,
                                            page=display_in_page).order_by('local_order')
        return banner_list

    def get_context_data(self, **kwargs):
        """
        :param kwargs:
        :return:
        """
        ctx = super(ProductCategoryView, self).get_context_data(**kwargs)
        ctx['best_selling_product'] = Product.get_best_selling_product(
            cat_id=None, num_product=self.num_products)
        ctx['right_banners'] = self.get_banner_list(group_order=0, banner_in_type='IBR', display_in_page='HOME')
        ctx['bottom_banners'] = self.get_banner_list(group_order=0, banner_in_type='IBB', display_in_page='HOME')
        ctx['list_categories'] = self.get_list_categories()
        ctx['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]
        return ctx


class ProductDetailView(CoreProductDetailView):
    template_folder = "%s/catalogue" % THEME
    num_products = 20
    num_enterprises = 20
    context_object_name = 'product'
    search_context_object_name = 'prds'

    def get(self, request, *args, **kwargs):
        try:
            enterprise_id = self.get_enterprise_id(request)
            self.search_handler = self.get_search_handler(
                request.GET, request.get_full_path(), enterprise_id)
        except InvalidPage:
            messages.error(request, _("The product doesn't have partner."))
        return super(ProductDetailView, self).get(request, *args, **kwargs)

    @staticmethod
    def get_banner_list(group_order, banner_in_type, display_in_page):
        banner_list = Banner.objects.filter(group_order=group_order, banner_in_type=banner_in_type, active=True,
                                            page=display_in_page).order_by('local_order')
        return banner_list

    def get_enterprise_id(self, request):
        product = self.get_object()
        if product.is_parent:
            product_info = request.strategy.fetch_for_parent(product)
        else:
            product_info = request.strategy.fetch_for_product(product)
        try:
            partner = product_info.stockrecord.partner
            if partner:
                return partner.id
        except AttributeError:
            return None
        return None

    # kiểm tra có tồn tại trong chuỗi không và lấy ra
    # def get_enterprise_id(self, request):
    #     partner_url = self.get_enterprise(request)
    #     url = partner_url.get_absolute_url()
    #     part = '(\_)+(\d+)'
    #     match = re.search(part, url)
    #     if match:
    #         partner_id = match.groups()[1]
    #     else:
    #         partner_id = None
    #     return partner_id

    def get_search_handler(self, *args, **kwargs):
        return EnterpriseProductSearchHandler(*args, **kwargs)

    def get_context_data(self, **kwargs):
        """
        :param kwargs:
        :return:
        """
        ctx = super(ProductDetailView, self).get_context_data(**kwargs)
        search_context = self.search_handler.get_search_context_data(
            self.search_context_object_name)
        ctx.update(search_context)
        ctx['best_selling_product'] = Product.get_best_selling_product(
            cat_id=None, num_product=self.num_products)
        ctx['bottom_banners'] = self.get_banner_list(group_order=0, banner_in_type='IBB', display_in_page='HOME')
        ctx['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]
        return ctx


class CatalogueView(CoreCatalogueView):
    template_name = '%s/catalogue/browse.html' % THEME
    num_products = 20
    num_enterprises = 20

    @staticmethod
    def get_banner_list(group_order, banner_in_type, display_in_page):
        banner_list = Banner.objects.filter(group_order=group_order, banner_in_type=banner_in_type, active=True,
                                            page=display_in_page).order_by('local_order')
        return banner_list

    def get_context_data(self, **kwargs):
        """
        :param kwargs:
        :return:
        """
        ctx = super(CatalogueView, self).get_context_data(**kwargs)
        ctx['best_selling_product'] = Product.get_best_selling_product(
            cat_id=None, num_product=self.num_products)
        ctx['bottom_banners'] = self.get_banner_list(group_order=0, banner_in_type='IBB', display_in_page='HOME')
        ctx['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]

        return ctx
