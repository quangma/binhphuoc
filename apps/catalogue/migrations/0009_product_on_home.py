# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0008_category_icon'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='on_home',
            field=models.BooleanField(default=False, db_index=True, verbose_name='Hi\u1ec7n tr\xean trang ch\u1ee7'),
        ),
    ]
