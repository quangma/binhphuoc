# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators
import oscar.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0005_auto_20150604_1450'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='is_active',
            field=models.BooleanField(default=True, db_index=True, verbose_name='Is_active'),
        ),
        migrations.AddField(
            model_name='category',
            name='templates',
            field=models.CharField(default=b'default', max_length=128, null=True, verbose_name='Templates'),
        ),
        migrations.AddField(
            model_name='product',
            name='is_active',
            field=models.BooleanField(default=True, db_index=True, verbose_name='Is_active'),
        ),
        migrations.AddField(
            model_name='product',
            name='short_description',
            field=models.TextField(max_length=512, null=True, verbose_name='Short description', blank=True),
        ),
        migrations.AlterField(
            model_name='productattribute',
            name='code',
            field=models.SlugField(max_length=128, verbose_name='Code', validators=[django.core.validators.RegexValidator(regex=b'^[a-zA-Z_][0-9a-zA-Z_]*$', message="Code can only contain the letters a-z, A-Z, digits, and underscores, and can't start with a digit"), oscar.core.validators.non_python_keyword]),
        ),
    ]
