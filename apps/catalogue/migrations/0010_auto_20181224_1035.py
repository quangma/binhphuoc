# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0009_product_on_home'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='on_home',
            field=models.BooleanField(default=True, db_index=True, verbose_name='Hi\u1ec7n tr\xean trang ch\u1ee7'),
        ),
    ]
