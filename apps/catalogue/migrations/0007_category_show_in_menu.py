# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0006_auto_20150703_1623'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='show_in_menu',
            field=models.BooleanField(default=True, db_index=True, verbose_name='Show in menu'),
        ),
    ]
