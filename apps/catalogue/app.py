# -*- coding: utf-8 -*-
from django.conf.urls import url
from oscar.apps.catalogue.app import CatalogueApplication as CoreCatApp


class CatalogueApplication(CoreCatApp):
    """
    """
    def get_urls(self):
        urlpatterns = super(CatalogueApplication, self).get_urls()
        urlpatterns += [
            url(r'^$', self.catalogue_view.as_view(), name='index'),
            url(r'^(?P<product_slug>[\w-]*)_(?P<pk>\d+)/$',
                self.detail_view.as_view(), name='detail'),
            # đổi đường dẫn cho thân thiện với người Việt
            url(r'^danh-muc/(?P<category_slug>[\w-]+(/[\w-]+)*)_(?P<pk>\d+)/$',
                self.category_view.as_view(), name='category'),
            # Fallback URL if a user chops of the last part of the URL
            url(r'^danh-muc/(?P<category_slug>[\w-]+(/[\w-]+)*)/$',
                self.category_view.as_view()),
            url(r'^nhom-san-pham/(?P<slug>[\w-]+)/$',
                self.range_view.as_view(), name='range')]
        return self.post_process_urls(urlpatterns)

application = CatalogueApplication()