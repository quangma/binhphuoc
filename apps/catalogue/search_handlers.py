# -*- coding: utf-8 -*-
from django.conf import settings

from oscar.core.loading import get_class, get_model

SearchHandler = get_class('search.search_handlers', 'SearchHandler')
BrowseCategoryForm = get_class('search.forms', 'BrowseCategoryForm')
Product = get_model('catalogue', 'Product')


class CustomProductSearchHandler(SearchHandler):
    """
    Search handler specialised for searching products.  Comes with optional
    category filtering. To be used with a Solr search backend.
    Vant <vantxm@gmail.com> rewrite c.name not full_name
    Because of categories in ProductIndex is category.name
    category.full_name : selected_facets is not correct
    """
    form_class = BrowseCategoryForm
    model_whitelist = [Product]
    paginate_by = settings.OSCAR_PRODUCTS_PER_PAGE

    def __init__(self, request_data, full_path, categories=None):
        self.categories = categories
        super(CustomProductSearchHandler, self).__init__(request_data, full_path)

    def get_search_queryset(self):
        sqs = super(CustomProductSearchHandler, self).get_search_queryset()
        if self.categories:
            # We use 'narrow' API to ensure Solr's 'fq' filtering is used as
            # opposed to filtering using 'q'.
            pattern = ' OR '.join([
                '"%s"' % c.name for c in self.categories])
            sqs = sqs.narrow('category_exact:(%s)' % pattern)
        return sqs