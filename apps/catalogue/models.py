# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404

from django.db import models
from django.utils.translation import ugettext_lazy as _

from oscar.core.loading import get_model
from oscar.apps.catalogue.abstract_models import AbstractCategory
from oscar.apps.catalogue.abstract_models import AbstractProduct
from oscar.apps.partner.strategy import Selector

ProductRecord = get_model('analytics', 'ProductRecord')
Line = get_model('order', 'Line')


class Category(AbstractCategory):
    """
    Ke thua tu Abstract Category
    Abstract Category:
    image: thuoc tinh dung de hien thi anh dai dien cho danh muc
    """
    is_active = models.BooleanField(verbose_name=_('Is_active'), db_index=True, default=True)
    templates = models.CharField(max_length=128, verbose_name=_("Templates"),
                                 null=True, default="default")
    icon = models.CharField(max_length=32, verbose_name=_("Icon"),
                            null=True, default="icon")
    show_in_menu = models.BooleanField(verbose_name=_('Show in menu'), db_index=True, default=True)

    def has_children(self):
        """
        @todo: dùng cache cho các hàm này
        """
        return self.get_num_children() > 0

    def get_num_children(self):
        """
        # @todo: dùng cache cho các hàm này
        """
        return self.get_children().count()

    def get_products(self):
        """
        return all product in this category
        Todo: dùng cache cho các hàm này
        """
        return Product.objects.filter(categories__pk=self.id).distinct()

    def get_num_products(self):
        """

        """
        return self.get_products().count()

    def get_total_products(self):
        """
        Todo: cache cho hàm này
        """
        descendants = self.get_descendants()
        descendants_id = [descendant.id for descendant in descendants]
        descendants_id.append(self.id)
        products = Product.objects.filter(categories__pk__in=descendants_id).distinct()
        return products

    def get_total_num_products(self):
        """
        Todo: cache cho hàm này
        "toi uu query bang cach chi lay id, id duoc danh index ,tranh su dung count(*)"
        """

        return self.get_total_products().only('id').count()

    def get_total_views(self):
        """
        return total views of child products
        """
        # def sum_num_views(x,y):
        # return x.num_views() + y.num_views()
        #
        # return reduce( sum_num_views, self.get_total_products())
        # bo di lam cham he thong kinh khung
        # list_num_views = [p.num_views() for p in self.get_total_products() if p.num_views() != 0]
        # return self.get_total_products().only('id').count()#sum(list_num_views)
        number_view = []
        for p in self.get_total_products().only('id'):
            try:
                product_record = ProductRecord.objects.get(product_id=p.id)
                number_view.append(product_record.num_views)
            except ProductRecord.DoesNotExist:
                a = 0
        return sum(number_view)

    def has_product(self):
        """
        """
        return self.get_total_num_products() > 0

    def get_active_children(self):
        """
        :returns: A queryset of all the node's children
        vant rewrite: get only active category
        """
        if self.is_leaf():
            return self.__class__.objects.none()
        return self.__class__.objects.filter(depth=self.depth + 1,
                                             path__range=self._get_children_path_interval(self.path),
                                             is_active=True).order_by('path')


class Product(AbstractProduct):
    """
    """
    short_description = models.TextField(_('Short description'), max_length=512,
                                         blank=True, null=True)
    is_active = models.BooleanField(verbose_name=_('Is_active'), db_index=True, default=True)
    on_home = models.BooleanField(verbose_name=_(u'Hiện trên trang chủ'), db_index=True, default=True)

    def num_views(self):
        """
        return num_views of the product
        """
        try:
            product_record = ProductRecord.objects.get(product__pk=self.id)
            return product_record.num_views
        except ProductRecord.DoesNotExist:
            return 0

    def get_category_names(self):
        """
        vant add
        Return a list of product's category title
        """
        cat_list = self.categories.all()
        cat_names = [cat.name for cat in cat_list]
        return cat_names

    @staticmethod
    def get_random_product(cat_id=None, num_product=5):
        """
        get random product by category
        :param cat_id:
        :param num_product:
        """
        if cat_id:
            return (Product.browsable.filter(categories__pk=cat_id)
                    .prefetch_related('images')
                    .order_by('?'))[:num_product]
        else:
            return (Product.browsable.all()
                    .order_by('?'))[:num_product]

    @staticmethod
    def get_recursive_random_product(cat_id=None, num_product=5):
        """
        get random product by category
        :param cat_id:
        :param num_product:
        """
        if cat_id:

            category = get_object_or_404(Category, pk=cat_id)
            categories = list(category.get_descendants().only('id'))
            categories.append(category)
            return Product.browsable.filter(categories__in=categories) \
                       .prefetch_related('images') \
                       .order_by('?')[:num_product]
        else:
            return Product.browsable.all() \
                       .prefetch_related('images') \
                       .order_by('?')[:num_product]

    @staticmethod
    def get_recursive_home_product(cat_id=None, num_product=5):
        """
        get random product by category
        :param cat_id:
        :param num_product:
        """
        if cat_id:

            category = get_object_or_404(Category, pk=cat_id)
            categories = list(category.get_descendants().only('id'))
            categories.append(category)
            return Product.browsable.filter(categories__in=categories, on_home=True) \
                       .prefetch_related('images') \
                       .order_by('?')[:num_product]
        else:
            return Product.browsable.all() \
                       .prefetch_related('images') \
                       .order_by('?')[:num_product]

    @staticmethod
    def get_feature_product(cat_id=None, num_product=5):
        """
        get feature product by category
        function doesn't implement
        :param cat_id:
        :param num_product:
        """
        if cat_id:
            category = get_object_or_404(Category, pk=cat_id)
            categories = list(category.get_descendants().only('id'))
            categories.append(category)
            return (Product.browsable.filter(categories__in=categories)
                    .prefetch_related('images')
                    .order_by('?'))[:num_product]
        else:
            return (Product.browsable.all()
                    .prefetch_related('images')
                    .order_by('?'))[:num_product]

    @staticmethod
    def get_best_selling_product(cat_id=None, num_product=5):
        """
        get best selling product by category
        @param: cat_id
        """
        slice = 0
        if cat_id:
            category = get_object_or_404(Category, pk=cat_id)
            categories = []
            if category.get_descendants():
                categories = list(category.get_descendants())
            categories.append(category)
            products = Product.objects.filter(categories__in=categories, is_active=True)
            child_products = []
            for product in products:
                child_products.append(Product.get_childproduct(product.id))
            five_lines = Line.objects.filter(product__in=products).order_by('product').distinct('product')
            child_arr = []
            for childs in child_products:
                for product in childs:
                    child_arr.append(product)

            additional_line = Line.objects.filter(product__in=child_arr).order_by('product').distinct('product')
            five_lines = list(five_lines) + list(additional_line)
            line_id = []
            for line in five_lines:
                line_id.append(line.id)

            line_products = Line.objects.filter(id__in=line_id).select_related('product').order_by('-quantity')[:20]
            products_return = []
            for line in line_products:
                products_return.append(line.product.id)
            if 5 > line_products.count() > 0:
                count = num_product - (line_products.count())
                result = list(Product.browsable.filter(categories__in=categories, is_active=True).
                              prefetch_related('images').
                              order_by('-productrecord__num_views')[:count])
                proids = [p.id for p in result]
                addition_views = products_return + proids
                return Product.objects.filter(id__in=addition_views).prefetch_related('images')[
                       :num_product]

            else:
                return Product.objects.filter(id__in=products_return, is_active=True).order_by('?')[:num_product]
        else:
            return (Product.browsable.all()
                    .prefetch_related('images')
                    .order_by('-line__quantity')[:num_product])

    @property
    def partner_name(self):
        """

        :return:
        """
        try:
            strategy = Selector().strategy()
            info = strategy.fetch_for_product(self)
            partner_name = info.stockrecord.partner.name
            return partner_name
        except AttributeError:
            return ''

    @property
    def partner_sumary(self):
        """

        :return:
        """
        try:
            strategy = Selector().strategy()
            info = strategy.fetch_for_product(self)
            partner_summary = info.stockrecord.partner.primary_address.summary
            return partner_summary
        except AttributeError:
            return ''


from oscar.apps.catalogue.models import *
from oscar.apps.analytics.models import *
