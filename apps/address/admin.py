from django.contrib import admin
from oscar.core.loading import get_model

admin.site.register(get_model('address', 'City'))
admin.site.register(get_model('address', 'District'))
admin.site.register(get_model('address', 'Ward'))

from oscar.apps.address.admin import *  # noqa
