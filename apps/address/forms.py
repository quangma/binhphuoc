# -*- coding: utf-8 -*-
from django import forms

from oscar.core.loading import get_model
from oscar.apps.address.forms import UserAddressForm as CoreUserAddressForm

UserAddress = get_model('address', 'useraddress')


class UserAddressForm(CoreUserAddressForm):
    address_id = forms.IntegerField(widget=forms.HiddenInput(), required=False, )

    def __init__(self, *args, **kwargs):
        super(UserAddressForm, self).__init__(*args, **kwargs)
        self.fields['line1'].widget.attrs['class'] = 'input_address'
        self.fields['line1'].widget.attrs['placeholder'] \
            = u'Số nhà, tên đường, tên tòa nhà (nếu có), phường xã, quận (huyện), tỉnh (thành phố)'
        self.fields['phone_number'].widget.attrs['class'] = 'input_phonenumber'
        self.fields['phone_number'].widget.attrs['placeholder'] = '+8409xxxxxxxx'
        self.fields['table_phone'].widget.attrs['class'] = 'input_tablenumber'
        # An mot so thong tin ve nuoc, ma vung trong form nhap lieu
        self.fields['country'].initial = 'VN'
        self.fields['country'].widget = forms.HiddenInput()
        self.fields['postcode'].initial = '100000'
        self.fields['postcode'].widget = forms.HiddenInput()

    class Meta:
        model = UserAddress
        fields = ('full_name', 'line1', 'email', 'phone_number', 'table_phone', 'postcode', 'country')
        exclude = ['title', 'first_name', 'last_name', 'line2', 'line3', 'line4',
                   'state', 'district', 'city', 'is_default_for_shipping']
