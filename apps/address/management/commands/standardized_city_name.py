# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand

from apps.address.models import City


def standardize_city_name(name):
    if u"Tỉnh" in name:
        return name.replace(u"Tỉnh ", '')
    if u"Thành phố" in name:
        return name.replace(u"Thành phố ", u"TP. ")
    return name


class Command(BaseCommand):
    help = "Standardized city name"

    def handle(self, *args, **options):
        cities = City.objects.all()
        for city in cities:
            name = standardize_city_name(city.name)
            city.name = name
            city.save()
        self.stdout.write('Successfully standardize city name')
