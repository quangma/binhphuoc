# -*- coding: utf-8 -*-

import openpyxl

from django.core.management.base import BaseCommand

from apps.address.models import City, District, Ward


def read_data(excel_file_name):
    """

    :param excel_file_name:
    :return:
    """
    wb = openpyxl.load_workbook(excel_file_name)
    sheet = wb.get_active_sheet()
    data = []
    for row in range(2, sheet.max_row + 1):
        city = sheet['A' + str(row)].value
        district = sheet['C' + str(row)].value
        ward = sheet['E' + str(row)].value
        data.append((city, district, ward))
    return data


def insert_db(data):
    """

    :param data:
    :return:
    """
    district_set = set()
    city_set = set()
    ward_set = set()
    for e in data:
        city, district, ward = e
        city_set.add(city)
        district_set.add((city, district))
        ward_set.add((district, ward))

    for c in city_set:
        city = City.objects.get_or_create(name=c)[0]
        city.save()
    for d in district_set:
        city_name, district_name = d
        city = City.objects.filter(name=city_name)[0]
        district = District(city=city, name=district_name)
        district.save()

    # for large wards object
    all_wards = []
    for w in ward_set:
        district_name, ward_name = w
        district = District.objects.filter(name=district_name)[0]
        ward = Ward(district=district, name=ward_name)
        all_wards.append(ward)
    # for sqlite
    #  bulk_create() "too many SQL variables" error
    batch_size = 30
    # batch_size = 500
    Ward.objects.bulk_create(all_wards, batch_size=batch_size)


class Command(BaseCommand):
    help = ("Populate Vietnam city and district to database.\n",
            "python manage.py populate_vietnam_data\n",
            " \"./aweb/apps/address/management/data/danh-sach-don-vi-hanh-chinh-vietnam-cap-nhat.xlsx\"")

    def add_arguments(self, parser):
        parser.add_argument('excel_file_name', nargs='+', type=str)

    def handle(self, *args, **options):
        excel_file_name = options['excel_file_name'][0]
        data = read_data(excel_file_name)
        insert_db(data)
        self.stdout.write('Successfully insert data from "%s" to database' % excel_file_name)
