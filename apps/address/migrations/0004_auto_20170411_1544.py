# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0003_auto_20170403_1401'),
    ]

    operations = [
        migrations.AlterField(
            model_name='useraddress',
            name='full_name',
            field=models.CharField(default=b'', max_length=50, verbose_name='H\u1ecd v\xe0 t\xean'),
        ),
    ]
