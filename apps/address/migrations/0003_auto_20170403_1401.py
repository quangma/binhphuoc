# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0002_city_district_ward'),
    ]

    operations = [
        migrations.AddField(
            model_name='useraddress',
            name='city',
            field=models.ForeignKey(verbose_name='T\u1ec9nh / Th\xe0nh Ph\u1ed1', blank=True, to='address.City', null=True),
        ),
        migrations.AddField(
            model_name='useraddress',
            name='district',
            field=models.ForeignKey(verbose_name='Qu\u1eadn / Huy\u1ec7n', blank=True, to='address.District', null=True),
        ),
        migrations.AddField(
            model_name='useraddress',
            name='email',
            field=models.EmailField(max_length=254, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='useraddress',
            name='full_name',
            field=models.CharField(default=b'', max_length=50),
        ),
        migrations.AddField(
            model_name='useraddress',
            name='table_phone',
            field=models.CharField(max_length=32, null=True, verbose_name='\u0110i\u1ec7n tho\u1ea1i c\u1ed1 \u0111\u1ecbnh', blank=True),
        ),
        migrations.AddField(
            model_name='useraddress',
            name='ward',
            field=models.ForeignKey(verbose_name='ph\u01b0\u1eddng/x\xe3', blank=True, to='address.Ward', null=True),
        ),
    ]
