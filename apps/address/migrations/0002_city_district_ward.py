# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=150, verbose_name='T\xean T\u1ec9nh/Th\xe0nh ph\u1ed1 ')),
                ('country', models.ForeignKey(related_name='country_city', verbose_name='Ch\u1ecdn qu\u1ed1c gia ', blank=True, to='address.Country', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='District',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=150, verbose_name='T\xean Qu\u1eadn/Huy\u1ec7n ')),
                ('city', models.ForeignKey(verbose_name='T\u1ec9nh/Th\xe0nh ph\u1ed1', blank=True, to='address.City', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Ward',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=150, verbose_name='T\xean Ph\u01b0\u1eddng/X\xe3')),
                ('district', models.ForeignKey(verbose_name='Qu\u1eadn/Huy\u1ec7n', blank=True, to='address.District', null=True)),
            ],
        ),
    ]
