# -*- coding: utf-8 -*-
import zlib
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from django.utils.translation import ugettext_lazy as _

from oscar.apps.address.abstract_models import AbstractPartnerAddress, AbstractUserAddress


class UserAddress(AbstractUserAddress):
    full_name = models.CharField(max_length=50, default='', verbose_name=_(u"Họ và tên"))
    table_phone = models.CharField(_(u"Điện thoại cố định"), max_length=32, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    city = models.ForeignKey('address.City', verbose_name=_(u"Tỉnh / Thành Phố"), null=True, blank=True)
    district = models.ForeignKey('address.District', verbose_name=_(u"Quận / Huyện"), null=True, blank=True)
    ward = models.ForeignKey('address.Ward', verbose_name=_(u"phường/xã"), null=True, blank=True)

    def active_address_fields(self, include_salutation=True):
        """
        Return the non-empty components of the address, but merging the
        title, first_name and last_name into a single line.
        vant override
        """
        fields = [self.line1, ]
        if self.phone_number:
            fields = [str(self.phone_number)] + fields
        if include_salutation:
            fields = [self.salutation] + fields
        fields = [f.strip() for f in fields if f]
        return fields

    def generate_hash(self):
        """
        Cập nhật hàm này để kiểm tránh lỗi insert nhiều địa chỉ gây lỗi
        IntegrityError: duplicate key value violates unique constraint "address_useraddress_user_id_*
        """
        hash_string = str(self.phone_number) + self.line1
        return zlib.crc32(hash_string.strip().upper().encode('UTF8'))


class PartnerAddress(AbstractPartnerAddress):
    """
        Địa chỉ của parter
        Trường hợp một partner có nhiều địa chỉ khác nhau,
        mỗi địa chỉ thông thường sẽ có các thông tin số điện thoại, fax khác nhau
    """
    city = models.ForeignKey('address.City', verbose_name=_(u"Tỉnh/Thành phố"), null=True, blank=True)
    district = models.ForeignKey('address.District', verbose_name=_(u"Quận/Huyện"), null=True, blank=True)
    ward = models.ForeignKey('address.Ward', verbose_name=_(u"Phường/Xã"), null=True, blank=True)

    home_phone = models.CharField(verbose_name=_(u"Điện thoại cố định"), blank=True, null=True, default="+84",
                                  max_length=128)
    mobile_phone = models.CharField(blank=True, verbose_name=_(u"Di động"), null=True, default="+84",
                                    max_length=128)
    fax_number = models.CharField(blank=True, verbose_name=_(u"Fax"), null=True,
                                  max_length=128)

    website = models.URLField(blank=True, verbose_name=_(u"Website"), null=True)
    email = models.EmailField(verbose_name=_(u"Email"), blank=True, null=True)

    def clean(self):
        # Strip all whitespace
        for field in ['first_name', 'last_name', 'line1', 'line2', 'line3',
                      'line4', 'state', 'postcode']:
            if self.__dict__[field]:
                self.__dict__[field] = self.__dict__[field].strip()

                # Ensure postcodes are valid for country
                # vant: remove postcode valid
                # self.ensure_postcode_is_valid_for_country()

    def all_address_fields(self, include_salutation=True):
        """
            Return the non-empty components of the address, but merging the
            title, first_name and last_name into a single line.
            """
        # fields = [self.line1, self.line2, self.line3,
        #           self.line4, self.state, self.postcode]
        # vant remove state and postcode
        fields = [self.line1, ]
        if include_salutation:
            fields = [self.salutation] + fields
        fields = [f.strip() for f in fields if f]
        if self.ward:
            fields.append(self.ward.name)
        if self.district:
            fields.append(self.district.name)
        if self.city:
            fields.append(self.city.name)
        return fields

    @property
    def summary(self):
        """
        Returns a single string summary of the address,
        separating fields using commas.
        """
        return u", ".join(self.all_address_fields())


class City(models.Model):
    country = models.ForeignKey('address.Country', verbose_name=_(u"Chọn quốc gia "),
                                related_name='country_city', null=True, blank=True)
    name = models.CharField(max_length=150, default='', verbose_name=_(u"Tên Tỉnh/Thành phố "))

    def __unicode__(self):
        return self.name

    @property
    def get_district_count(self):
        return District.objects.filter(city=self.id).count()


class District(models.Model):
    city = models.ForeignKey('address.City', verbose_name=_(u"Tỉnh/Thành phố"), null=True, blank=True)
    name = models.CharField(max_length=150, default='', verbose_name=_(u"Tên Quận/Huyện "))

    def __unicode__(self):
        return self.name

    @property
    def get_ward_count(self):
        return Ward.objects.filter(district=self.id).count()


class Ward(models.Model):
    district = models.ForeignKey('address.District', verbose_name=_(u"Quận/Huyện"), null=True, blank=True)
    name = models.CharField(max_length=150, default='', verbose_name=_(u"Tên Phường/Xã"))

    def __unicode__(self):
        return self.name


from oscar.apps.address.models import *  # noqa
