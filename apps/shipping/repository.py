# -*- coding: utf-8 -*-
from decimal import Decimal as D

from oscar.apps.shipping import repository, methods as core_methods
from django.utils.translation import ugettext_lazy as _


class Standard(core_methods.Free):
    code = 'CHUAN'
    name = _(u"Chuyển phát thông thường (liên hệ với doanh nghiệp cung cấp sản phẩm)")


class Netco(core_methods.FixedPrice):
    code = 'NC'
    name = _(u"Chuyển phát nhanh (liên hệ với doanh nghiệp cung cấp sản phẩm)")


class Repository(repository.Repository):
    methods = (Standard(), Netco(D('00000.00'), D('00000.00')))