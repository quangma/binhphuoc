from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class DocumentConfig(AppConfig):
    label = 'document'
    name = 'apps.document'
    verbose_name = _('Document')
