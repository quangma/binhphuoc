# -*- coding: utf-8 -*-
from datetime import date

from django.core.urlresolvers import reverse
from django.db import models
from django.core.cache import cache
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from oscar.core.utils import slugify

from .settings import LEGAL_DOCUMENT_FILE_FOLDER


class LegalDocument(models.Model):
    """
    Model cho Văn bản quy phạm pháp luật
    Các thông tin lấy từ
    http://vbpl.vn/Pages/portal.aspx
    """
    EFF, EXP = ('Eff', 'Exp')
    STATUS_CHOICES = (
        (EFF, _(u"Còn hiệu lực")),
        (EXP, _(u"Hết hiệu lực")),
    )

    CONS, CODE, LAW, ORDI, ORDE, RESO, JRES, DECR, DECI, CIRC, JCIR = (
        'Constitution', 'Code', 'Law', 'Ordinance', 'Order', 'Resolution',
        'Joint Resolution', 'Decree', 'Decision', 'Circular', 'Joint circular')

    LEGISLATION_CHOICES = (
        (CONS, _(u"Hiến pháp")),
        (CODE, _(u"Bộ luật")),
        (LAW, _(u"Luật")),
        (ORDI, _(u"Pháp lệnh")),
        (ORDE, _(u"Lệnh")),
        (RESO, _(u"Nghị quyết")),
        (JRES, _(u"Nghị quyết liên tịch")),
        (DECR, _(u"Nghị định")),
        (DECI, _(u"Quyết định")),
        (CIRC, _(u"Thông tư")),
        (JCIR, _(u"Thông tư liên tịch")),
    )
    # http://vbpl.vn/TW/Pages/cacboen.aspx
    # http://vbpl.vn/TW/Pages/cacbo.aspx
    ISSUING_BODY_CHOICES = (
        ('State President', _(u"Chủ tịch nước")),
        ('The Government', _(u"Chính phủ")),
        ('Prime Minister', _(u"Thủ tướng Chính phủ")),
        ('Ministry of Industry and Trade', _(u"Bộ Công Thương")),
        ('Ministry of Planning and Investment', _(u"Bộ Kế hoạch và Đầu tư")),
        ('The Ministry of Science and Technology', _(u"Bộ Khoa học và Công nghệ")),
    )

    OFFICIAL_NUMBER_PATTERN = '__/____/____-____'
    title = models.CharField(_(u'Tiêu đề'),
                             max_length=255, blank=True)
    official_number = models.CharField(_(u"Số ký hiệu"), max_length=128,
                                       default=OFFICIAL_NUMBER_PATTERN, unique=True, blank=False)
    status = models.CharField(_(u"Hiệu lực văn bản"),
                              max_length=3, choices=STATUS_CHOICES, blank=True)
    slug = models.SlugField(_('Slug'), max_length=255, unique=False)
    description = models.TextField(_('Description'), blank=True)
    content = models.TextField(_('Content'), blank=True)

    date_created = models.DateTimeField(_("Date created"), auto_now_add=True)
    date_updated = models.DateTimeField(
        _("Date updated"), auto_now=True, db_index=True)
    published_date = models.DateField(_(u"Ngày ban hành"), blank=True, null=True)
    validated_date = models.DateField(_(u"Ngày hiệu lực"), blank=True, null=True)

    legislation_type = models.CharField(_(u"Loại văn bản"),
                                        max_length=128, choices=LEGISLATION_CHOICES, blank=True)
    issuing_body = models.CharField(_(u"Đơn vị ban hành"),
                                    max_length=128, choices=ISSUING_BODY_CHOICES, blank=True)

    signer_name = models.CharField(_(u'Người ký'),
                                   max_length=255, blank=True)
    signer_position = models.CharField(_(u'Chức vụ'),
                                       max_length=255, blank=True)

    file = models.FileField(_(u'Tệp đính kèm'), upload_to=LEGAL_DOCUMENT_FILE_FOLDER, blank=True)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super(LegalDocument, self).save(*args, **kwargs)

    @staticmethod
    def get_latest_by_issue(issuing_body=None, num=20):
        """
        """
        if issuing_body:
            return LegalDocument.objects.filter(issuing_body=issuing_body).order_by('-published_created')[:num]
        else:
            return LegalDocument.objects.all().order_by('-published_created')[:num]

    @staticmethod
    def get_latest_by_type(legislation_type=None, num=20):
        """
        """
        if legislation_type:
            return LegalDocument.objects.filter(legislation_type=legislation_type).order_by('-published_created')[:num]
        else:
            return LegalDocument.objects.all().order_by('-published_created')[:num]

    def get_absolute_url(self):
        """
        Return a document's absolute url
        """
        cache_key = 'LEGAL_DOCUMENT_URL_%s' % self.pk
        url = cache.get(cache_key)
        if not url:
            url = reverse(
                'document:detail',
                kwargs={'legal_document_slug': self.slug, 'pk': self.pk})
            cache.set(cache_key, url)
        return url

    def __unicode__(self):
        return self.title

    def is_expired(self):
        if not self.validated_date:
            return True
        if date.today() >= self.validated_date:
            return False
        return True

    def get_other_documents(self, num=5):
        """
        :param num:
        :return:
        """
        return LegalDocument.objects.filter(~Q(id=self.id)).order_by('-date_updated')[:num]

    class Meta:
        app_label = 'document'
        ordering = ['-published_date']
        verbose_name = _('LegalDocument')
        verbose_name_plural = _('LegalDocument')