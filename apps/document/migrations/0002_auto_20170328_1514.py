# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='legaldocument',
            name='published_date',
            field=models.DateField(null=True, verbose_name='Ng\xe0y ban h\xe0nh', blank=True),
        ),
        migrations.AlterField(
            model_name='legaldocument',
            name='validated_date',
            field=models.DateField(null=True, verbose_name='Ng\xe0y hi\u1ec7u l\u1ef1c', blank=True),
        ),
    ]
