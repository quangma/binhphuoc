# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LegalDocument',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Ti\xeau \u0111\u1ec1', blank=True)),
                ('official_number', models.CharField(default=b'__/____/____-____', unique=True, max_length=128, verbose_name='S\u1ed1 k\xfd hi\u1ec7u')),
                ('status', models.CharField(blank=True, max_length=3, verbose_name='Hi\u1ec7u l\u1ef1c v\u0103n b\u1ea3n', choices=[(b'Eff', 'C\xf2n hi\u1ec7u l\u1ef1c'), (b'Exp', 'H\u1ebft hi\u1ec7u l\u1ef1c')])),
                ('slug', models.SlugField(max_length=255, verbose_name='Slug')),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('content', models.TextField(verbose_name='Content', blank=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='Date created')),
                ('date_updated', models.DateTimeField(auto_now=True, verbose_name='Date updated', db_index=True)),
                ('published_date', models.DateField(verbose_name='Ng\xe0y ban h\xe0nh', blank=True)),
                ('validated_date', models.DateField(verbose_name='Ng\xe0y hi\u1ec7u l\u1ef1c', blank=True)),
                ('legislation_type', models.CharField(blank=True, max_length=128, verbose_name='Lo\u1ea1i v\u0103n b\u1ea3n', choices=[(b'Constitution', 'Hi\u1ebfn ph\xe1p'), (b'Code', 'B\u1ed9 lu\u1eadt'), (b'Law', 'Lu\u1eadt'), (b'Ordinance', 'Ph\xe1p l\u1ec7nh'), (b'Order', 'L\u1ec7nh'), (b'Resolution', 'Ngh\u1ecb quy\u1ebft'), (b'Joint Resolution', 'Ngh\u1ecb quy\u1ebft li\xean t\u1ecbch'), (b'Decree', 'Ngh\u1ecb \u0111\u1ecbnh'), (b'Decision', 'Quy\u1ebft \u0111\u1ecbnh'), (b'Circular', 'Th\xf4ng t\u01b0'), (b'Joint circular', 'Th\xf4ng t\u01b0 li\xean t\u1ecbch')])),
                ('issuing_body', models.CharField(blank=True, max_length=128, verbose_name='\u0110\u01a1n v\u1ecb ban h\xe0nh', choices=[(b'State President', 'Ch\u1ee7 t\u1ecbch n\u01b0\u1edbc'), (b'The Government', 'Ch\xednh ph\u1ee7'), (b'Prime Minister', 'Th\u1ee7 t\u01b0\u1edbng Ch\xednh ph\u1ee7'), (b'Ministry of Industry and Trade', 'B\u1ed9 C\xf4ng Th\u01b0\u01a1ng'), (b'Ministry of Planning and Investment', 'B\u1ed9 K\u1ebf ho\u1ea1ch v\xe0 \u0110\u1ea7u t\u01b0'), (b'The Ministry of Science and Technology', 'B\u1ed9 Khoa h\u1ecdc v\xe0 C\xf4ng ngh\u1ec7')])),
                ('signer_name', models.CharField(max_length=255, verbose_name='Ng\u01b0\u1eddi k\xfd', blank=True)),
                ('signer_position', models.CharField(max_length=255, verbose_name='Ch\u1ee9c v\u1ee5', blank=True)),
                ('file', models.FileField(upload_to=b'document/files/%Y/%m/', verbose_name='T\u1ec7p \u0111\xednh k\xe8m', blank=True)),
            ],
            options={
                'ordering': ['-published_date'],
                'verbose_name': 'LegalDocument',
                'verbose_name_plural': 'LegalDocument',
            },
        ),
    ]
