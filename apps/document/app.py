from django.conf.urls import url

from oscar.core.application import Application
from oscar.core.loading import get_class


class DocumentApplication(Application):
    name = 'document'
    detail_view = get_class('document.views', 'DocumentDetailView')
    document_view = get_class('document.views', 'DocumentView')

    def get_urls(self):
        urlpatterns = super(DocumentApplication, self).get_urls()
        urlpatterns += [
            url(r'^$', self.document_view.as_view(), name='index'),
            url(r'^(?P<legal_document_slug>[\w-]*)_(?P<pk>\d+)/$',
                self.detail_view.as_view(), name='detail'),
            # url(r'^danh-muc/(?P<category_slug>[\w-]+(/[\w-]+)*)_(?P<pk>\d+)/$',
            #     self.news_category_view.as_view(), name='category'),
        ]
        return self.post_process_urls(urlpatterns)


application = DocumentApplication()
