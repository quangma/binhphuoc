from django.contrib import admin
from oscar.core.loading import get_model

LegalDocument = get_model('document', 'LegalDocument')


class LegalDocumentAdmin(admin.ModelAdmin):
    date_hierarchy = 'date_created'


admin.site.register(LegalDocument, LegalDocumentAdmin)
