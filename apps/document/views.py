# -*- coding:utf-8 -*-

from django.conf import settings
from django.views.generic import ListView, DetailView

from oscar.core.loading import get_model

if settings.THEMES:
    THEME = settings.THEMES[0]

LegalDocument = get_model('document', 'LegalDocument')
Partner = get_model('partner', 'Partner')
Banner = get_model('promotions', 'Banner')


class DocumentView(ListView):
    model = LegalDocument
    ordering = '-published_date'
    paginate_by = 20

    template_name = '%s/document/all-legal-document.html' % THEME
    num_enterprises = 20

    @staticmethod
    def get_banner_list(group_order, banner_in_type, display_in_page):
        banner_list = Banner.objects.filter(group_order=group_order, banner_in_type=banner_in_type, active=True,
                                            page=display_in_page).order_by('local_order')
        return banner_list

    def get_context_data(self, **kwargs):
        ctx = super(DocumentView, self).get_context_data(**kwargs)
        ctx['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]
        ctx['news_banners'] = self.get_banner_list(group_order=0, banner_in_type='IBP', display_in_page='NEWS')
        return ctx


class DocumentDetailView(DetailView):
    model = LegalDocument
    context_object_name = 'document'
    template_name = '%s/document/legal-document-detail.html' % THEME
    num_enterprises = 20

    def get_context_data(self, **kwargs):
        ctx = super(DocumentDetailView, self).get_context_data(**kwargs)
        ctx['other_documents'] = self.get_object().get_other_documents()
        ctx['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]
        return ctx
