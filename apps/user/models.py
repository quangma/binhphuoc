# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext as _


class Profile(models.Model):
    """
    Profile model for customer
    """
    MALE, FEMALE = 'M', 'F'
    gender_choices = (
        (MALE, _(u'Nam')),
        (FEMALE, _(u'Nữ')))

    user = models.OneToOneField('auth.User', related_name="profile")
    gender = models.CharField(max_length=1, choices=gender_choices,
                              verbose_name=_(u'Giới tính'), null=True)
    birth_of_date = models.DateTimeField(verbose_name=_(u'Ngày sinh'),
                                         null=True, blank=True,
                                         help_text=_(u'Bạn nhập ngày sinh theo cú pháp: năm-tháng-ngày'))
    phone_number = models.CharField(max_length=24, verbose_name=_(u'Số điện thoại'), null=True, blank=True)

    address = models.CharField(verbose_name=_(u'Địa chỉ'), null=True, default='', max_length=256, blank=True)
