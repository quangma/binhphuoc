# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('gender', models.CharField(max_length=1, null=True, verbose_name='Gi\u1edbi t\xednh', choices=[(b'M', 'Nam'), (b'F', 'N\u1eef')])),
                ('birth_of_date', models.DateTimeField(null=True, verbose_name='Ng\xe0y sinh', blank=True)),
                ('phone_number', models.CharField(max_length=24, null=True, verbose_name='S\u1ed1 \u0111i\u1ec7n tho\u1ea1i', blank=True)),
                ('address', models.CharField(default=b'', max_length=256, null=True, verbose_name='\u0110\u1ecba ch\u1ec9', blank=True)),
                ('user', models.OneToOneField(related_name='profile', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
