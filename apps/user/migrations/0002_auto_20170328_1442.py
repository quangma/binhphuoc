# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='birth_of_date',
            field=models.DateTimeField(help_text='B\u1ea1n nh\u1eadp ng\xe0y sinh theo c\xfa ph\xe1p: n\u0103m-th\xe1ng-ng\xe0y', null=True, verbose_name='Ng\xe0y sinh', blank=True),
        ),
    ]
