# -*- coding: utf-8 -*-
import zlib
from django.db import models
from django.utils.translation import ugettext_lazy as _

from oscar.apps.address.abstract_models import AbstractShippingAddress


class ShippingAddress(AbstractShippingAddress):
    full_name = models.CharField(max_length=50, default='Nguyen Van A', verbose_name=_(u"Họ và tên") )
    table_phone = models.CharField(_(u"Điện thoại cố định"), max_length=32, blank=True, null=True)
    email = models.EmailField(blank=True)
    city = models.ForeignKey('address.City', verbose_name=_(u"Tỉnh / Thành Phố"), null=True, blank=True)
    district = models.ForeignKey('address.District', verbose_name=_(u"Quận / Huyện"), null=True, blank=True)
    ward = models.ForeignKey('address.Ward', verbose_name=_(u"phường/xã"), null=True, blank=True)

    def active_address_fields(self, include_salutation=True):
        """
        Return the non-empty components of the address, but merging the
        title, first_name and last_name into a single line.
        vant override
        """
        fields = [self.line1, ]
        if self.phone_number:
            fields = [str(self.phone_number)] + fields
        if include_salutation:
            fields = [self.salutation] + fields
        fields = [f.strip() for f in fields if f]
        return fields

    def generate_hash(self):
        """
        Cập nhật hàm này để kiểm tránh lỗi insert nhiều địa chỉ gây lỗi
        IntegrityError: duplicate key value violates unique constraint "address_useraddress_user_id_*
        """
        hash_string = str(self.phone_number) + self.line1
        return zlib.crc32(hash_string.strip().upper().encode('UTF8'))

from oscar.apps.order.models import *
