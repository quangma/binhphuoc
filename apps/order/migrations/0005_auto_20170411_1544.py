# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0004_auto_20170403_1147'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shippingaddress',
            name='full_name',
            field=models.CharField(default=b'Nguyen Van A', max_length=50, verbose_name='H\u1ecd v\xe0 t\xean'),
        ),
        migrations.AlterField(
            model_name='shippingaddress',
            name='table_phone',
            field=models.CharField(max_length=32, null=True, verbose_name='\u0110i\u1ec7n tho\u1ea1i c\u1ed1 \u0111\u1ecbnh', blank=True),
        ),
    ]
