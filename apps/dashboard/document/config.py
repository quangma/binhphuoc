from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class DocumentDashboardConfig(AppConfig):
    label = 'document_dashboard'
    name = 'apps.dashboard.document'
    verbose_name = _('Document')