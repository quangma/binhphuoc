# -*- coding: utf-8 -*-
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.views import generic
from django.utils.translation import ugettext_lazy as _

from django_tables2 import SingleTableMixin
from oscar.core.loading import get_model, get_class
from oscar.views.generic import ObjectLookupView


LegalDocument = get_model('document', 'LegalDocument')
LegalDocumentTable = get_class('dashboard.document.tables', 'LegalDocumentTable')
LegalDocumentForm = get_class('dashboard.document.forms', 'LegalDocumentForm')
LegalDocumentSearchForm = get_class('dashboard.document.forms', 'LegalDocumentSearchForm')


class LegalDocumentListView(SingleTableMixin, generic.TemplateView):
    template_name = 'dashboard/document/document_item_list.html'
    table_class = LegalDocumentTable
    form_class = LegalDocumentSearchForm
    context_table_name = 'document_items'

    def get_context_data(self, **kwargs):
            ctx = super(LegalDocumentListView, self).get_context_data(**kwargs)
            ctx['form'] = self.form
            return ctx

    def get_queryset(self):
        queryset = LegalDocument.objects.all().order_by('-date_updated')
        queryset = self.apply_search(queryset)
        return queryset

    def apply_search(self, queryset):
            """
            Filter the queryset and set the description according to the search
            parameters given
            """
            self.form = self.form_class(self.request.GET)

            if not self.form.is_valid():
                return queryset

            data = self.form.cleaned_data

            if data.get('title'):
                queryset = queryset.filter(title__icontains=data['title'])
            return queryset


class LegalDocumentListMixin(object):

    def get_success_url(self):
        return reverse("dashboard:document-item-list")


class LegalDocumentCreateView(LegalDocumentListMixin, generic.CreateView):
    template_name = 'dashboard/document/document_item_form.html'
    model = LegalDocument
    form_class = LegalDocumentForm

    def get_context_data(self, **kwargs):
        ctx = super(LegalDocumentCreateView, self).get_context_data(**kwargs)
        ctx['title'] = _("Add a document item")
        return ctx

    def get_success_url(self):
        messages.info(self.request, _("LegalDocument created successfully"))
        return super(LegalDocumentCreateView, self).get_success_url()


class LegalDocumentUpdateView(LegalDocumentListMixin, generic.UpdateView):
    template_name = 'dashboard/document/document_item_form.html'
    model = LegalDocument
    form_class = LegalDocumentForm

    def get_context_data(self, **kwargs):
        ctx = super(LegalDocumentUpdateView, self).get_context_data(**kwargs)
        ctx['title'] = _("Update item '%s'") % self.object.title
        return ctx

    def get_success_url(self):
        messages.info(self.request, _("LegalDocument updated successfully"))
        return super(LegalDocumentUpdateView, self).get_success_url()


class LegalDocumentDeleteView(LegalDocumentListMixin, generic.DeleteView):
    template_name = 'dashboard/document/document_item_delete.html'
    model = LegalDocument

    def get_context_data(self, *args, **kwargs):
        ctx = super(LegalDocumentDeleteView, self).get_context_data(*args, **kwargs)
        return ctx

    def get_success_url(self):
        messages.info(self.request, _("LegalDocument Category deleted successfully"))
        return super(LegalDocumentDeleteView, self).get_success_url()


class LegalDocumentLookupView(ObjectLookupView):
    model = LegalDocument

    def get_queryset(self):
        return self.model.ojbects.all()

    def lookup_filter(self, qs, term):
        return qs.filter(title__icontains=term)