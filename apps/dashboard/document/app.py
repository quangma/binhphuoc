from django.conf.urls import url

from oscar.core.application import Application
from oscar.core.loading import get_class


class LegalDocumentApplication(Application):
    name = None

    default_permissions = ['is_staff', ]

    document_list_view = get_class("dashboard.document.views",
                                   "LegalDocumentListView")
    document_lookup_view = get_class("dashboard.document.views",
                                     "LegalDocumentLookupView")
    document_create_view = get_class("dashboard.document.views",
                                     "LegalDocumentCreateView")
    document_update_view = get_class("dashboard.document.views",
                                     "LegalDocumentUpdateView")
    document_delete_view = get_class("dashboard.document.views",
                                     "LegalDocumentDeleteView")

    def get_urls(self):
        urls = [
            url(r'^item/(?P<pk>\d+)/$',
                self.document_update_view.as_view(),
                name='document-item-update'),
            url(r'^item/create/$',
                self.document_create_view.as_view(),
                name='document-item-create'),
            url(r'^item/(?P<pk>\d+)/delete/$',
                self.document_delete_view.as_view(),
                name='document-item-delete'),
            url(r'^$', self.document_list_view.as_view(),
                name='document-item-list'),
        ]
        return self.post_process_urls(urls)


application = LegalDocumentApplication()
