from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _, ungettext_lazy
from django_tables2 import LinkColumn, TemplateColumn, A, Column, DateColumn
from oscar.core.loading import get_class, get_model

DashboardTable = get_class('dashboard.tables', 'DashboardTable')
LegalDocument = get_model('document', 'LegalDocument')


class LegalDocumentTable(DashboardTable):
    title = LinkColumn('document:detail', attrs={'target': '_blank'}, args=[A('slug'), A('pk')])
    official_number = Column()
    legislation_type = Column()
    issuing_body = Column()
    published_date = DateColumn()
    validated_date = DateColumn()

    actions = TemplateColumn(
        verbose_name=_('Actions'),
        template_name='dashboard/document/document_item_row_actions.html',
        orderable=False)

    icon = "file-text"
    caption = ungettext_lazy("%s LegalDocument", "%s LegalDocument")

    class Meta(DashboardTable.Meta):
        model = LegalDocument
        fields = ('title', 'official_number', 'legislation_type',
                  'issuing_body', 'published_date', 'validated_date')
        order_by = '-published_date'
