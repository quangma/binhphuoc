from django import forms
from django.utils.translation import ugettext_lazy as _

from oscar.core.loading import get_model


LegalDocument = get_model('document', 'LegalDocument')


class LegalDocumentForm(forms.ModelForm):
    class Meta:
        model = LegalDocument
        fields = ['title', 'official_number', 'description', 'content', 'published_date', 'validated_date',
                  'legislation_type', 'issuing_body', 'signer_name', 'signer_position', 'file']


class LegalDocumentSearchForm(forms.Form):
    title = forms.CharField(max_length=255, required=False, label=_('Title'))