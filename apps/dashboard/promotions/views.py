# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.views import generic

from django_tables2 import SingleTableMixin

from oscar.core.loading import get_model

from .forms import BannerForm, BannerSearchForm, VideoForm, VideoSearchForm
from .tables import BannerTable, VideoTable

Banner = get_model('promotions', 'Banner')
Video = get_model('promotions', 'Video')


class BannerListView(SingleTableMixin, generic.TemplateView):
    template_name = 'dashboard/promotions/banner_list.html'
    table_class = BannerTable
    form_class = BannerSearchForm
    context_table_name = 'banners'

    def get_context_data(self, **kwargs):
        ctx = super(BannerListView, self).get_context_data(**kwargs)
        ctx['form'] = self.form
        return ctx

    def get_queryset(self):
        queryset = Banner.objects.all().order_by('page')
        queryset = self.apply_search(queryset)
        return queryset

    def apply_search(self, queryset):
        """
            Filter the queryset and set the description according to the search
            parameters given
            """
        self.form = self.form_class(self.request.GET)

        if not self.form.is_valid():
            return queryset

        data = self.form.cleaned_data

        if data.get('title'):
            queryset = queryset.filter(title__icontains=data['title'])
        return queryset


class BannerUpdateView(generic.UpdateView):
    model = Banner
    form_class = BannerForm
    template_name = 'dashboard/promotions/banner_update.html'

    def get_success_url(self):
        return reverse('dashboard:banner-list')


class BannerCreateView(generic.CreateView):
    model = Banner
    form_class = BannerForm
    template_name = 'dashboard/promotions/banner_create.html'

    def form_valid(self, form):
        form_object = form.save(commit=False)
        form_object.user = self.request.user
        form_object.save()
        return super(BannerCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('dashboard:banner-list')


class BannerDeleteView(generic.DeleteView):
    model = Banner
    template_name = 'dashboard/promotions/banner_delete.html'

    def get_context_data(self, **kwargs):
        ctx = {'banner': self.model.objects.get(pk=self.kwargs['pk'])}
        return ctx

    def get_success_url(self):
        return reverse('dashboard:banner-list')


class VideoListView(SingleTableMixin, generic.TemplateView):
    template_name = 'dashboard/promotions/video_list.html'
    table_class = VideoTable
    form_class = VideoSearchForm
    context_table_name = 'videos'

    def get_context_data(self, **kwargs):
        ctx = super(VideoListView, self).get_context_data(**kwargs)
        ctx['form'] = self.form
        return ctx

    def get_queryset(self):
        queryset = Video.objects.all().order_by('-date_updated')
        queryset = self.apply_search(queryset)
        return queryset

    def apply_search(self, queryset):
        """
            Filter the queryset and set the description according to the search
            parameters given
            """
        self.form = self.form_class(self.request.GET)

        if not self.form.is_valid():
            return queryset

        data = self.form.cleaned_data

        if data.get('title'):
            queryset = queryset.filter(title__icontains=data['title'])
        return queryset


class VideoUpdateView(generic.UpdateView):
    model = Video
    form_class = VideoForm
    template_name = 'dashboard/promotions/video_update.html'

    def get_success_url(self):
        return reverse('dashboard:video-list')


class VideoCreateView(generic.CreateView):
    model = Video
    form_class = VideoForm
    template_name = 'dashboard/promotions/video_create.html'

    def form_valid(self, form):
        form_object = form.save(commit=False)
        form_object.user = self.request.user
        form_object.save()
        return super(VideoCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('dashboard:video-list')


class VideoDeleteView(generic.DeleteView):
    model = Video
    template_name = 'dashboard/promotions/video_delete.html'

    def get_context_data(self, **kwargs):
        ctx = {'video': self.model.objects.get(pk=self.kwargs['pk'])}
        return ctx

    def get_success_url(self):
        return reverse('dashboard:video-list')
