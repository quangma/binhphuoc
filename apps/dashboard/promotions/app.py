# -*- coding: utf-8 -*-
from django.conf.urls import url
from oscar.core.loading import get_class
from oscar.apps.dashboard.promotions.app import PromotionsDashboardApplication as CorePromotionsDashboardApplication


class PromotionsDashboardApplication(CorePromotionsDashboardApplication):
    banner_list = get_class('dashboard.promotions.views', 'BannerListView')
    banner_update = get_class('dashboard.promotions.views', 'BannerUpdateView')
    banner_create = get_class('dashboard.promotions.views', 'BannerCreateView')
    banner_delete = get_class('dashboard.promotions.views', 'BannerDeleteView')

    video_list = get_class('dashboard.promotions.views', 'VideoListView')
    video_update = get_class('dashboard.promotions.views', 'VideoUpdateView')
    video_create = get_class('dashboard.promotions.views', 'VideoCreateView')
    video_delete = get_class('dashboard.promotions.views', 'VideoDeleteView')

    def get_urls(self):
        urls = super(PromotionsDashboardApplication, self).get_urls()
        urls = urls + [
            url(r'^banner-list/$', self.banner_list.as_view(), name="banner-list"),
            url(r'^banner-create/$', self.banner_create.as_view(), name="banner-create"),
            url(r'^banner-update/(?P<pk>\d+)/$', self.banner_update.as_view(), name="banner-update"),
            url(r'^banner-delete/(?P<pk>\d+)/$', self.banner_delete.as_view(), name="banner-delete"),
            url(r'^video-list/$', self.video_list.as_view(), name="video-list"),
            url(r'^video-create/$', self.video_create.as_view(), name="video-create"),
            url(r'^video-update/(?P<pk>\d+)/$', self.video_update.as_view(), name="video-update"),
            url(r'^video-delete/(?P<pk>\d+)/$', self.video_delete.as_view(), name="video-delete"),
        ]
        return self.post_process_urls(urls)

application = PromotionsDashboardApplication()