# -*- coding: utf-8 -*-

__author__ = 'vant'

# from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _, ungettext_lazy
from django_tables2 import LinkColumn, TemplateColumn, A, Column, BooleanColumn
from oscar.core.loading import get_class, get_model


DashboardTable = get_class('dashboard.tables', 'DashboardTable')
Banner = get_model('promotions', 'Banner')
Video = get_model('promotions', 'Video')


class BannerTable(DashboardTable):
    title = LinkColumn('dashboard:banner-update', attrs={'target': '_blank'}, args=[A('pk')], )
    banner = TemplateColumn(
        verbose_name=_('Image'),
        template_name='dashboard/promotions/banner_row_image.html',
        orderable=False)
    banner_in_type = Column(verbose_name=_(u'Loại banner'))
    group_order = Column(verbose_name=_(u'Thuộc nhóm thứ #'))
    local_order = Column(verbose_name=_(u'Thứ tự hiển thị trong nhóm'))
    page = Column(verbose_name=_(u'Trang hiển thị'))

    active = BooleanColumn(verbose_name=_(u'Hiển thị'))
    actions = TemplateColumn(
        verbose_name=_('Actions'),
        template_name='dashboard/promotions/banner_row_actions.html',
        orderable=False)

    icon = "sitemap"
    caption = ungettext_lazy("%s Banner", "%s Banner")

    class Meta(DashboardTable.Meta):
        model = Banner
        fields = ('title', 'banner', 'page', 'banner_in_type', 'group_order', 'local_order', 'active')
        order_by = ('-page', 'banner_in_type')


class VideoTable(DashboardTable):
    title = LinkColumn('dashboard:video-update', attrs={'target': '_blank'}, args=[A('pk')], )
    video = Column(verbose_name=_(u'Tệp video'))
    url = Column(verbose_name=_(u'Địa chỉ URL'))
    date_created = Column(verbose_name=_(u'Ngày tạo'))
    date_updated = Column(verbose_name=_(u'Ngày cập nhật'))

    actions = TemplateColumn(
        verbose_name=_('Actions'),
        template_name='dashboard/promotions/video_row_actions.html',
        orderable=False)

    icon = "sitemap"
    caption = ungettext_lazy("%s Video", "%s Video")

    class Meta(DashboardTable.Meta):
        model = Video
        fields = ('title', 'video', 'url', 'date_created', 'date_updated')
        order_by = ('-date_updated', '-date_created')
