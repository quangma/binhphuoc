# -*- coding:utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from oscar.core.loading import get_model

Banner = get_model('promotions', 'Banner')
Video = get_model('promotions', 'Video')


class BannerForm(forms.ModelForm):

    class Meta:
        model = Banner
        exclude = ('user',)


class BannerSearchForm(forms.Form):
    title = forms.CharField(max_length=255, required=False, label=_(u'Tên video'))


class VideoForm(forms.ModelForm):

    class Meta:
        model = Video
        fields = ('title', 'video', 'url',)


class VideoSearchForm(forms.Form):
    title = forms.CharField(max_length=255, required=False, label=_(u'Tên video'))