# -*- coding: utf-8 -*-
from django.conf.urls import url

from oscar.core.loading import get_class

from oscar.apps.dashboard.users.app import UserManagementApplication as CoreUserManagementApplication


class UserManagementApplication(CoreUserManagementApplication):
    default_permissions = ['is_staff', ]

    partner_user_view = get_class('dashboard.users.views', 'PartnerUserView')

    def get_urls(self):
        urls = super(UserManagementApplication, self).get_urls()
        urls += [
            url(r'^entp/$', self.partner_user_view.as_view(),
                name='partner-user-list'), ]
        return self.post_process_urls(urls)


application = UserManagementApplication()
