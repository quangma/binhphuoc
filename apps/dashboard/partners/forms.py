# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import pgettext_lazy
from django.utils.translation import ugettext_lazy as _

from oscar.core.loading import get_model
from oscar.apps.dashboard.partners.forms import PartnerAddressForm as CorePartnerAddressForm

from apps.address.models import District, Ward

PartnerAddress = get_model('partner', 'PartnerAddress')
Partner = get_model('partner', 'Partner')


class PartnerAddressForm(CorePartnerAddressForm):
    name = forms.CharField(
        required=False, label=pgettext_lazy(u"Partner's name", u"Name"))

    class Meta:
        fields = ('name', 'home_phone', 'mobile_phone', 'email', 'fax_number',
                  'website', 'line1', 'country', 'city', 'district', 'ward')
        model = PartnerAddress


class PartnerCreateForm(forms.ModelForm):
    class Meta:
        model = Partner
        fields = ('name', 'type',
                  'category', 'logo',
                  'banner', 'about',
                  'active',)


class PartnerSearchForm(forms.Form):
    name = forms.CharField(
        required=False, label=pgettext_lazy(u"Partner's name", u"Name"))
    district = forms.ModelChoiceField(queryset=District.objects.all(),
                                      required=False,
                                      label=_(u"Quận/Huyện"))

    ward = forms.ModelChoiceField(queryset=Ward.objects.all(),
                                  required=False,
                                  label=_(u"Phường/Xã"))