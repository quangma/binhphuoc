# -*- coding: utf-8 -*-
from oscar.views import sort_queryset

__author__ = 'vant'
from django.views import generic
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy, reverse

from oscar.core.loading import get_class
from oscar.core.loading import get_model
from oscar.apps.dashboard.partners.views import PartnerListView as CorePartnerListView


Partner = get_model('partner', 'Partner')
PartnerAddressForm = get_class('dashboard.partners.forms', 'PartnerAddressForm')
PartnerCreateForm = get_class('dashboard.partners.forms', 'PartnerCreateForm')
PartnerSearchForm = get_class('dashboard.partners.forms', 'PartnerSearchForm')


class PartnerUpdateView(generic.UpdateView):
    model = Partner
    form_class = PartnerCreateForm
    template_name = 'dashboard/partners/partner_form.html'
    success_url = reverse_lazy('dashboard:partner-list')

    def get_context_data(self, **kwargs):
        ctx = super(PartnerUpdateView, self).get_context_data(**kwargs)
        # for upload image
        ctx['includes_files'] = True
        return ctx

    def get_success_url(self):
        messages.success(self.request,
                         _("Partner '%s' was updated successfully.") %
                         self.object.name)
        return reverse('dashboard:partner-list')


class PartnerListView(CorePartnerListView):
    paginate_by = 50

    def get_queryset(self):
        qs = self.model._default_manager.all()
        qs = sort_queryset(qs, self.request, ['name'])

        self.description = _("All partners")

        # We track whether the queryset is filtered to determine whether we
        # show the search form 'reset' button.
        self.is_filtered = False
        self.form = self.form_class(self.request.GET)
        if not self.form.is_valid():
            return qs

        data = self.form.cleaned_data

        if data.get('name'):
            qs = qs.filter(name__icontains=data['name'])
            self.is_filtered = True
        if data.get('district'):
            qs = qs.filter(addresses__district=data['district'])
            self.is_filtered = True
        if data.get('ward'):
            qs = qs.filter(addresses__ward=data['ward'])
            self.is_filtered = True
        if data.get('type'):
            qs = qs.filter(type=data['type'])
            self.is_filtered = True
        if self.is_filtered:
            self.description = _("Kết quả tìm kiếm")

        return qs

