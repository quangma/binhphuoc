# -*- coding: utf-8 -*-
__author__ = 'vant'
from django.conf.urls import url

from oscar.core.loading import get_class

from oscar.apps.dashboard.partners.app import PartnersDashboardApplication as CorePartnersDashboardApplication


class PartnersDashboardApplication(CorePartnersDashboardApplication):
    update_view = get_class('dashboard.partners.views', 'PartnerUpdateView')

    def get_urls(self):
        urls = super(PartnersDashboardApplication, self).get_urls()
        urls += [
            url(r'^(?P<pk>\d+)/update/$', self.update_view.as_view(),
                name='partner-update'), ]
        return self.post_process_urls(urls)

application = PartnersDashboardApplication()