# -*- coding: utf-8 -*-
from oscar.apps.dashboard.catalogue.app import CatalogueApplication as CoreCatalogueApplication


class CatalogueApplication(CoreCatalogueApplication):
    """
    """
    pass


application = CatalogueApplication()
