# -*- coding: utf-8 -*-
"""
Created on Mar 15, 2013
Changed on Dec 08, 2014
Changed on Feb 25, 2015
Changed on Aug 31, 2015
@author: vant
"""

from django import forms
from django.forms.models import inlineformset_factory
from django.utils.translation import ugettext_lazy as _
from treebeard.forms import movenodeform_factory

from ckeditor_uploader.widgets import CKEditorUploadingWidget

from oscar.core.loading import get_model
from oscar.apps.dashboard.catalogue.forms import ProductForm as CoreProductForm
from oscar.apps.dashboard.catalogue.forms import StockRecordForm as CoreStockRecordForm

Product = get_model('catalogue', 'Product')
ProductClass = get_model('catalogue', 'ProductClass')
ProductAttribute = get_model('catalogue', 'ProductAttribute')
AttributeOptionGroup = get_model('catalogue', 'AttributeOptionGroup')
AttributeOption = get_model('catalogue', 'AttributeOption')
Category = get_model('catalogue', 'Category')
StockRecord = get_model('partner', 'StockRecord')


class StockRecordForm(CoreStockRecordForm):
    quantity_add_stock = forms.IntegerField(required=False, label=_(u'Thêm số lượng vào kho'))

    class Meta:
        model = StockRecord
        exclude = ('product', 'num_allocated', 'price_currency', 'num_in_stock',)


class ProductAttributeForm(forms.ModelForm):
    class Meta:
        model = ProductAttribute
        exclude = ('entity_type',)


class AttributeOptionGroupSearchForm(forms.Form):
    name = forms.CharField(max_length=255, required=False, label=_('Name'))
    product_class = forms.ModelChoiceField(queryset=ProductClass.objects.all(),
                                           required=False, label=_('Product Class'))


class AttributeOptionGroupForm(forms.ModelForm):
    """

    """

    class Meta:
        model = AttributeOptionGroup
        fields = ('name',)


AttributeOptionFormSet = inlineformset_factory(AttributeOptionGroup, AttributeOption,
                                               exclude=(), extra=5, can_delete=True)


class ProductSearchForm(forms.Form):
    upc = forms.CharField(max_length=16, required=False, label=_('UPC'))
    title = forms.CharField(max_length=255, required=False, label=_('Title'))
    category = forms.ModelChoiceField(queryset=Category.objects.all(),
                                      required=False, label=_('Category'))


class UpdateProductActiveForm(forms.ModelForm):
    is_active = forms.BooleanField()

    def save(self, is_active, commit=True):
        instance = super(UpdateProductActiveForm, self).save(commit=False)

        if commit:
            # save
            # for django>=1.5
            # instance.save(update_fields=['name'])
            instance.update(is_active=is_active)

        return instance

    class Meta:
        model = Product
        fields = ('is_active',)


class ProductForm(CoreProductForm):
    class Meta:
        model = Product
        exclude = ('slug', 'status', 'score',
                   'is_discountable',
                   'recommended_products', 'product_options',
                   # 'related_products',
                   'structure', 'parent',
                   'attributes', 'categories',
                   'meta_title', 'meta_keywords', 'meta_description')

        widgets = {
            'short_description': CKEditorUploadingWidget,
            'description': CKEditorUploadingWidget,
        }


class AttributeSearchForm(forms.Form):
    """
    Form tim kiem cac thuoc tinh theo tieu chi
    @params: name ten thuoc tinh
    @params: category
    """
    name = forms.CharField(max_length=255, required=False, label=_('Name'))
    product_class = forms.ModelChoiceField(queryset=ProductClass.objects.all(),
                                           required=False, label=_('Product Class'))


CategoryForm = movenodeform_factory(
    Category,
    fields=['name', 'description', 'image', 'is_active', 'show_in_menu'])
