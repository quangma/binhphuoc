from ckeditor_uploader.widgets import CKEditorUploadingWidget

from oscar.core.loading import get_model
from oscar.apps.dashboard.pages.forms import PageUpdateForm\
    as CorePageUpdateForm

FlatPage = get_model('flatpages', 'FlatPage')


class PageUpdateForm(CorePageUpdateForm):

    class Meta:
        model = FlatPage
        fields = ('title', 'url', 'content')
        widgets = {
            'content': CKEditorUploadingWidget,
        }
