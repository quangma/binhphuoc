# -*- coding: utf-8 -*-
from oscar.apps.dashboard.pages.app import FlatPageManagementApplication \
    as CoreFlatPageManagementApplication


class FlatPageManagementApplication(CoreFlatPageManagementApplication):
    pass


application = FlatPageManagementApplication()
