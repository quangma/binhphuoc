from oscar.apps.dashboard.pages.views import PageCreateView as CorePageCreateView
from oscar.apps.dashboard.pages.views import PageUpdateView as CorePageUpdateView

from .forms import PageUpdateForm


class PageCreateView(CorePageCreateView):
    form_class = PageUpdateForm


class PageUpdateView(CorePageUpdateView):
    """
    View for updating flatpages from the dashboard.
    """
    form_class = PageUpdateForm


