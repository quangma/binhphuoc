# -*- coding:utf-8 -*-
from django.conf.urls import url, include

from oscar.core.loading import get_class
from oscar.apps.dashboard.app import DashboardApplication as CoreDashboardApplication


class DashboardApplication(CoreDashboardApplication):
    """
    """
    news_app = get_class('dashboard.news.app', 'application')
    document_app = get_class('dashboard.document.app', 'application')

    def get_urls(self):
        urls = super(DashboardApplication, self).get_urls()
        urls += [
            url(r'^news/', include(self.news_app.urls)),
            url(r'^document/', include(self.document_app.urls)),
        ]
        return self.post_process_urls(urls)


application = DashboardApplication()