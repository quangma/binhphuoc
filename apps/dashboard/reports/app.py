from django.conf.urls import url

from oscar.apps.dashboard.reports.app import ReportsApplication as CoreReportsApplication

class ReportsApplication(CoreReportsApplication):
    pass


application = ReportsApplication()
