from django import forms
from django.utils.translation import ugettext_lazy as _

from treebeard.forms import movenodeform_factory
from ckeditor_uploader.widgets import CKEditorUploadingWidget

from oscar.core.loading import get_model

NewsCategory = get_model('news', 'NewsCategory')
News = get_model('news', 'News')

NewsCategoryForm = movenodeform_factory(
    NewsCategory,
    fields=['name', 'description', 'image'])


class NewsForm(forms.ModelForm):
    class Meta:
        model = News
        fields = ['category', 'title', 'description', 'content', 'image', ]
        widgets = {
            'description': CKEditorUploadingWidget,
            'content': CKEditorUploadingWidget,
        }


class NewsSearchForm(forms.Form):
    title = forms.CharField(max_length=255, required=False, label=_('Title'))
    category = forms.ModelChoiceField(queryset=NewsCategory.objects.all(),
                                      required=False, label=_('NewsCategory'))
