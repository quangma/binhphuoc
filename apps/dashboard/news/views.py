# -*- coding: utf-8 -*-
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.views import generic
from django.utils.translation import ugettext_lazy as _

from django_tables2 import SingleTableMixin
from oscar.core.loading import get_model, get_class
from oscar.views.generic import ObjectLookupView


News = get_model('news', 'News')
NewsCategory = get_model('news', 'NewsCategory')
NewsCategoryTable = get_class('dashboard.news.tables', 'NewsCategoryTable')
NewsTable = get_class('dashboard.news.tables', 'NewsTable')
NewsCategoryForm = get_class('dashboard.news.forms', 'NewsCategoryForm')
NewsForm = get_class('dashboard.news.forms', 'NewsForm')
NewsSearchForm = get_class('dashboard.news.forms', 'NewsSearchForm')


class NewsCategoryListView(SingleTableMixin, generic.TemplateView):
    template_name = 'dashboard/news/news_category_list.html'
    table_class = NewsCategoryTable
    context_table_name = 'news_categories'

    def get_queryset(self):
        return NewsCategory.get_root_nodes()

    def get_context_data(self, *args, **kwargs):
        ctx = super(NewsCategoryListView, self).get_context_data(*args, **kwargs)
        ctx['child_categories'] = NewsCategory.get_root_nodes()
        return ctx


class NewsCategoryDetailListView(SingleTableMixin, generic.DetailView):
    template_name = 'dashboard/news/news_category_list.html'
    model = NewsCategory
    context_object_name = 'news_category'
    table_class = NewsCategoryTable
    context_table_name = 'news_categories'

    def get_table_data(self):
        return self.object.get_children()

    def get_context_data(self, *args, **kwargs):
        ctx = super(NewsCategoryDetailListView, self).get_context_data(*args,
                                                                   **kwargs)
        ctx['child_categories'] = self.object.get_children()
        ctx['ancestors'] = self.object.get_ancestors_and_self()
        return ctx


class NewsCategoryListMixin(object):

    def get_success_url(self):
        parent = self.object.get_parent()
        if parent is None:
            return reverse("dashboard:news-category-list")
        else:
            return reverse("dashboard:news-category-detail-list",
                           args=(parent.pk,))


class NewsCategoryCreateView(NewsCategoryListMixin, generic.CreateView):
    template_name = 'dashboard/news/news_category_form.html'
    model = NewsCategory
    form_class = NewsCategoryForm

    def get_context_data(self, **kwargs):
        ctx = super(NewsCategoryCreateView, self).get_context_data(**kwargs)
        ctx['title'] = _("Add a news category")
        return ctx

    def get_success_url(self):
        messages.info(self.request, _("NewsCategory created successfully"))
        return super(NewsCategoryCreateView, self).get_success_url()

    def get_initial(self):
        # set child category if set in the URL kwargs
        initial = super(NewsCategoryCreateView, self).get_initial()
        if 'parent' in self.kwargs:
            initial['_ref_node_id'] = self.kwargs['parent']
        return initial


class NewsCategoryUpdateView(NewsCategoryListMixin, generic.UpdateView):
    template_name = 'dashboard/news/news_category_form.html'
    model = NewsCategory
    form_class = NewsCategoryForm

    def get_context_data(self, **kwargs):
        ctx = super(NewsCategoryUpdateView, self).get_context_data(**kwargs)
        ctx['title'] = _("Update category '%s'") % self.object.name
        return ctx

    def get_success_url(self):
        messages.info(self.request, _("NewsCategory updated successfully"))
        return super(NewsCategoryUpdateView, self).get_success_url()


class NewsCategoryDeleteView(NewsCategoryListMixin, generic.DeleteView):
    template_name = 'dashboard/news/news_category_delete.html'
    model = NewsCategory

    def get_context_data(self, *args, **kwargs):
        ctx = super(NewsCategoryDeleteView, self).get_context_data(*args, **kwargs)
        ctx['parent'] = self.object.get_parent()
        return ctx

    def get_success_url(self):
        messages.info(self.request, _("News Category deleted successfully"))
        return super(NewsCategoryDeleteView, self).get_success_url()


class NewsListView(SingleTableMixin, generic.TemplateView):
    template_name = 'dashboard/news/news_item_list.html'
    table_class = NewsTable
    form_class = NewsSearchForm
    context_table_name = 'news_items'

    def get_context_data(self, **kwargs):
            ctx = super(NewsListView, self).get_context_data(**kwargs)
            ctx['form'] = self.form
            return ctx

    def get_queryset(self):
        queryset = News.objects.all().order_by('-date_updated')
        queryset = self.apply_search(queryset)
        return queryset

    def apply_search(self, queryset):
            """
            Filter the queryset and set the description according to the search
            parameters given
            """
            self.form = self.form_class(self.request.GET)

            if not self.form.is_valid():
                return queryset

            data = self.form.cleaned_data

            if data.get('title'):
                queryset = queryset.filter(title__icontains=data['title'])
            if data.get('category'):
                queryset = queryset.filter(category=data['category'])
            return queryset


class NewsListMixin(object):

    def get_success_url(self):
        return reverse("dashboard:news-item-list")


class NewsCreateView(NewsListMixin, generic.CreateView):
    template_name = 'dashboard/news/news_item_form.html'
    model = News
    form_class = NewsForm

    def get_context_data(self, **kwargs):
        ctx = super(NewsCreateView, self).get_context_data(**kwargs)
        ctx['title'] = _("Add a news item")
        return ctx

    def get_success_url(self):
        messages.info(self.request, _("News created successfully"))
        return super(NewsCreateView, self).get_success_url()


class NewsUpdateView(NewsListMixin, generic.UpdateView):
    template_name = 'dashboard/news/news_item_form.html'
    model = News
    form_class = NewsForm

    def get_context_data(self, **kwargs):
        ctx = super(NewsUpdateView, self).get_context_data(**kwargs)
        ctx['title'] = _("Update item '%s'") % self.object.title
        return ctx

    def get_success_url(self):
        messages.info(self.request, _("News updated successfully"))
        return super(NewsUpdateView, self).get_success_url()


class NewsDeleteView(NewsListMixin, generic.DeleteView):
    template_name = 'dashboard/news/news_item_delete.html'
    model = News

    def get_context_data(self, *args, **kwargs):
        ctx = super(NewsDeleteView, self).get_context_data(*args, **kwargs)
        return ctx

    def get_success_url(self):
        messages.info(self.request, _("News Category deleted successfully"))
        return super(NewsDeleteView, self).get_success_url()


class NewsLookupView(ObjectLookupView):
    model = News

    def get_queryset(self):
        return self.model.ojbects.all()

    def lookup_filter(self, qs, term):
        return qs.filter(title__icontains=term)