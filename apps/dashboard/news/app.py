from django.conf.urls import url

from oscar.core.application import Application
from oscar.core.loading import get_class


class NewsApplication(Application):
    name = None

    default_permissions = ['is_staff', ]

    news_list_view = get_class('dashboard.news.views',
                               'NewsListView')
    news_lookup_view = get_class('dashboard.news.views',
                                 'NewsLookupView')
    news_create_view = get_class('dashboard.news.views',
                                 'NewsCreateView')
    news_update_view = get_class('dashboard.news.views',
                                 'NewsUpdateView')
    news_delete_view = get_class('dashboard.news.views',
                                 'NewsDeleteView')

    category_list_view = get_class('dashboard.news.views',
                                   'NewsCategoryListView')
    category_detail_list_view = get_class('dashboard.news.views',
                                          'NewsCategoryDetailListView')
    category_create_view = get_class('dashboard.news.views',
                                     'NewsCategoryCreateView')
    category_update_view = get_class('dashboard.news.views',
                                     'NewsCategoryUpdateView')
    category_delete_view = get_class('dashboard.news.views',
                                     'NewsCategoryDeleteView')

    def get_urls(self):
        urls = [
            url(r'^item/(?P<pk>\d+)/$',
                self.news_update_view.as_view(),
                name='news-item-update'),
            url(r'^item/create/$',
                self.news_create_view.as_view(),
                name='news-item-create'),
            url(r'^item/(?P<pk>\d+)/delete/$',
                self.news_delete_view.as_view(),
                name='news-item-delete'),
            url(r'^$', self.news_list_view.as_view(),
                name='news-item-list'),

            url(r'^categories/$', self.category_list_view.as_view(),
                name='news-category-list'),
            url(r'^categories/(?P<pk>\d+)/$',
                self.category_detail_list_view.as_view(),
                name='news-category-detail-list'),
            url(r'^categories/create/$', self.category_create_view.as_view(),
                name='news-category-create'),
            url(r'^categories/create/(?P<parent>\d+)$',
                self.category_create_view.as_view(),
                name='news-category-create-child'),
            url(r'^categories/(?P<pk>\d+)/update/$',
                self.category_update_view.as_view(),
                name='news-category-update'),
            url(r'^categories/(?P<pk>\d+)/delete/$',
                self.category_delete_view.as_view(),
                name='news-category-delete'),
        ]
        return self.post_process_urls(urls)


application = NewsApplication()
