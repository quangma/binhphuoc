from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class NewsDashboardConfig(AppConfig):
    label = 'news_dashboard'
    name = 'apps.dashboard.news'
    verbose_name = _('News')