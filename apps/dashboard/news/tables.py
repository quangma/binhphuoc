from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _, ungettext_lazy
from django_tables2 import LinkColumn, TemplateColumn, A
from oscar.core.loading import get_class, get_model

DashboardTable = get_class('dashboard.tables', 'DashboardTable')
NewsCategory = get_model('news', 'NewsCategory')
News = get_model('news', 'News')


class NewsCategoryTable(DashboardTable):
    name = LinkColumn('dashboard:news-category-update', args=[A('pk')])
    description = TemplateColumn(
        template_code='{{ record.description|default:""|striptags'
                      '|cut:"&nbsp;"|truncatewords:6 }}')
    # mark_safe is needed because of
    # https://github.com/bradleyayers/django-tables2/issues/187
    num_children = LinkColumn(
        'dashboard:news-category-detail-list', args=[A('pk')],
        verbose_name=mark_safe(_('Number of child categories')),
        accessor='get_num_children',
        orderable=False)
    actions = TemplateColumn(
        verbose_name=_('Actions'),
        template_name='dashboard/news/news_category_row_actions.html',
        orderable=False)

    icon = "sitemap"
    caption = ungettext_lazy("%s NewsCategory", "%s Categories")

    class Meta(DashboardTable.Meta):
        model = NewsCategory
        fields = ('name', 'description')


class NewsTable(DashboardTable):
    title = LinkColumn('news:detail', attrs={'target': '_blank'}, args=[A('slug'), A('pk')])
    image = TemplateColumn(
        verbose_name=_('Image'),
        template_name='dashboard/news/news_row_image.html',
        orderable=False)
    category = TemplateColumn(
        verbose_name=_('Category'),
        template_name='dashboard/news/news_item_row_category.html')
    description = TemplateColumn(
        template_code='{{ record.description|default:""|striptags'
                      '|cut:"&nbsp;"|truncatewords:30 }}')

    actions = TemplateColumn(
        verbose_name=_('Actions'),
        template_name='dashboard/news/news_item_row_actions.html',
        orderable=False)

    icon = "sitemap"
    caption = ungettext_lazy("%s News", "%s News")

    class Meta(DashboardTable.Meta):
        model = News
        fields = ('title', 'image', 'category', 'description')
        order_by = '-date_updated'
