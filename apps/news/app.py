from django.conf.urls import url

from oscar.core.application import Application
from oscar.core.loading import get_class


class NewsApplication(Application):
    name = 'news'
    detail_view = get_class('news.views', 'NewsDetailView')
    news_view = get_class('news.views', 'NewsView')
    news_category_view = get_class('news.views', 'NewsCategoryView')

    def get_urls(self):
        urlpatterns = super(NewsApplication, self).get_urls()
        urlpatterns += [
            url(r'^$', self.news_view.as_view(), name='index'),
            url(r'^(?P<news_slug>[\w-]*)_(?P<pk>\d+)/$',
                self.detail_view.as_view(), name='detail'),
            url(r'^danh-muc/(?P<category_slug>[\w-]+(/[\w-]+)*)_(?P<pk>\d+)/$',
                self.news_category_view.as_view(), name='category'),
        ]
        return self.post_process_urls(urlpatterns)


application = NewsApplication()