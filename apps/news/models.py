# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.db import models
from django.core.cache import cache
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _, pgettext_lazy

from oscar.apps.catalogue.abstract_models import AbstractCategory
from oscar.core.utils import slugify

from .settings import NEWS_IMAGE_FOLDER


class NewsCategory(AbstractCategory):
    """
    Danh mục tin: kế thừa từ class danh mục sản phẩm
    """
    is_active = models.BooleanField(verbose_name=_('Is_active'), db_index=True, default=True)
    templates = models.CharField(max_length=128, verbose_name=_("Templates"),
                                 null=True, default="default")

    class Meta:
        abstract = False
        app_label = 'news'
        ordering = ['path']
        verbose_name = _('NewsCategory')
        verbose_name_plural = _('NewsCategories')

    def get_absolute_url(self):
            """
            Our URL scheme means we have to look up the category's ancestors. As
            that is a bit more expensive, we cache the generated URL. That is
            safe even for a stale cache, as the default implementation of
            ProductCategoryView does the lookup via primary key anyway. But if
            you change that logic, you'll have to reconsider the caching
            approach.
            """
            cache_key = 'NEWS_CATEGORY_URL_%s' % self.pk
            url = cache.get(cache_key)
            if not url:
                url = reverse(
                    'news:category',
                    kwargs={'category_slug': self.full_slug, 'pk': self.pk})
                cache.set(cache_key, url)
            return url


class News(models.Model):
    """
    Model cho Tin tức
    """
    category = models.ForeignKey(NewsCategory)
    title = models.CharField(pgettext_lazy(u'News title', u'Title'),
                             max_length=255, blank=True)
    slug = models.SlugField(_('Slug'), max_length=255, unique=False)
    description = models.TextField(_('Description'), blank=True)
    image = models.ImageField(_('Image'), upload_to=NEWS_IMAGE_FOLDER, max_length=255)
    content = models.TextField(_('Content'), blank=True)

    date_created = models.DateTimeField(_("Date created"), auto_now_add=True)
    date_updated = models.DateTimeField(
        _("Date updated"), auto_now=True, db_index=True)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super(News, self).save(*args, **kwargs)

    @staticmethod
    def get_recursive_latest_news(cat_id=None, num_news=5):
        """
        get lastest news by category
        :param cat_id:
        :param num_news:
        """
        if cat_id:

            category = get_object_or_404(NewsCategory, pk=cat_id)
            categories = list(category.get_descendants().only('id'))
            categories.append(category)
            return News.objects.filter(category__in=categories).order_by('-date_updated')[:num_news]
        else:
            return News.objects.all().order_by('-date_updated')[:num_news]

    @staticmethod
    def get_recursive_random_news(cat_id=None, num_news=5):
        """
        get random news by category
        :param cat_id:
        :param num_news:
        """
        if cat_id:

            category = get_object_or_404(NewsCategory, pk=cat_id)
            categories = list(category.get_descendants().only('id'))
            categories.append(category)
            return list(set((News.objects.filter(category__in=categories)
                             .order_by('?'))[:num_news]))
        else:
            return list(set((News.objects.all()
                             .order_by('?'))[:num_news]))

    def get_absolute_url(self):
        """
        Return a news's absolute url
        """
        return reverse('news:detail',
                       kwargs={'news_slug': self.slug, 'pk': self.id})

    def get_other_news(self, num_news=5):
        """
        :param num_news:
        :return:
        """
        return list(set((News.objects.filter(category=self.category).exclude(id=self.id)
                             .order_by('-date_updated'))[:num_news]))

    class Meta:
            abstract = False
            app_label = 'news'
            ordering = ['-date_updated']
            verbose_name = _('News')
            verbose_name_plural = _('News')