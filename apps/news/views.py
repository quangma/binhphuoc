# -*- coding:utf-8 -*-
import warnings
from django.conf import settings
from django.core.checks import messages
from django.core.paginator import InvalidPage
from django.http import Http404
from django.shortcuts import redirect, get_object_or_404
from django.views.generic import DetailView, TemplateView
from django.utils.translation import ugettext_lazy as _

from oscar.core.loading import get_model, get_class


if settings.THEMES:
    THEME = settings.THEMES[0]
News = get_model('news', 'News')
NewsCategory = get_model('news', 'NewsCategory')
SimpleNewsSearchHandler = get_class('news.search_handlers', 'SimpleNewsSearchHandler')
Partner = get_model('partner', 'Partner')
Banner = get_model('promotions', 'Banner')


class NewsDetailView(DetailView):
    context_object_name = 'news'
    model = News
    template_name = '%s/news/detail.html' % THEME
    num_enterprises = 20

    @staticmethod
    def get_banner_list(group_order, banner_in_type, display_in_page):
        banner_list = Banner.objects.filter(group_order=group_order, banner_in_type=banner_in_type, active=True,
                                            page=display_in_page).order_by('local_order')
        return banner_list

    def get_context_data(self, **kwargs):
        ctx = super(NewsDetailView, self).get_context_data(**kwargs)
        ctx['other_news'] = self.get_object().get_other_news()
        ctx['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]
        ctx['news_banners'] = self.get_banner_list(group_order=0, banner_in_type='IBP', display_in_page='NEWS')
        return ctx


class NewsView(TemplateView):
    """
    Browse all news in the site
    """
    context_object_name = "news"
    template_name = '%s/news/all-news.html' % THEME
    num_enterprises = 20

    @staticmethod
    def get_banner_list(group_order, banner_in_type, display_in_page):
        banner_list = Banner.objects.filter(group_order=group_order, banner_in_type=banner_in_type, active=True,
                                            page=display_in_page).order_by('local_order')
        return banner_list

    def get(self, request, *args, **kwargs):
        try:
            self.search_handler = SimpleNewsSearchHandler(
                self.request.GET, request.get_full_path(), [])
        except InvalidPage:
            # Redirect to page one.
            messages.error(request, _('The given page number was invalid.'))
            return redirect(self.category.get_absolute_url())
        return super(NewsView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(NewsView, self).get_context_data(**kwargs)
        search_context = self.search_handler.get_search_context_data(
            self.context_object_name)
        context['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]
        context['news_banners'] = self.get_banner_list(group_order=0, banner_in_type='IBP', display_in_page='NEWS')
        context.update(search_context)
        return context


class NewsCategoryView(TemplateView):
    context_object_name = "news"
    template_name = '%s/news/category-news.html' % THEME
    num_enterprises = 20

    @staticmethod
    def get_banner_list(group_order, banner_in_type, display_in_page):
        banner_list = Banner.objects.filter(group_order=group_order, banner_in_type=banner_in_type, active=True,
                                            page=display_in_page).order_by('local_order')
        return banner_list

    def get(self, request, *args, **kwargs):
        self.category = self.get_category()
        try:
            self.search_handler = SimpleNewsSearchHandler(
                self.request.GET, request.get_full_path(), self.get_categories())
        except InvalidPage:
            # Redirect to page one.
            messages.error(request, _('The given page number was invalid.'))
            return redirect(self.category.get_absolute_url())
        return super(NewsCategoryView, self).get(request, *args, **kwargs)

    def get_category(self):
        if 'pk' in self.kwargs:
            # Usual way to reach a category page. We just look at the primary
            # key, which is easy on the database. If the slug changed, get()
            # will redirect appropriately.
            # WARNING: Category.get_absolute_url needs to look up it's parents
            # to compute the URL. As this is slightly expensive, Oscar's
            # default implementation caches the method. That's pretty safe
            # as ProductCategoryView does the lookup by primary key, which
            # will work even if the cache is stale. But if you override this
            # logic, consider if that still holds true.
            return get_object_or_404(NewsCategory, pk=self.kwargs['pk'])
        elif 'category_slug' in self.kwargs:
            # DEPRECATED. TODO: Remove in Oscar 1.2.
            # For SEO and legacy reasons, we allow chopping off the primary
            # key from the URL. In that case, we have the target category slug
            # and it's ancestors' slugs concatenated together.
            # To save on queries, we pick the last slug, look up all matching
            # categories and only then compare.
            # Note that currently we enforce uniqueness of slugs, but as that
            # might feasibly change soon, it makes sense to be forgiving here.
            concatenated_slugs = self.kwargs['category_slug']
            slugs = concatenated_slugs.split(NewsCategory._slug_separator)
            try:
                last_slug = slugs[-1]
            except IndexError:
                raise Http404
            else:
                for category in NewsCategory.objects.filter(slug=last_slug):
                    if category.full_slug == concatenated_slugs:
                        message = (
                            "Accessing categories without a primary key"
                            " is deprecated will be removed in Oscar 1.2.")
                        warnings.warn(message, DeprecationWarning)

                        return category

        raise Http404

    def get_categories(self):
        """
        Return a list of the current category and its ancestors
        """
        return self.category.get_descendants_and_self()

    def get_context_data(self, **kwargs):
        context = super(NewsCategoryView, self).get_context_data(**kwargs)
        context['category'] = self.category
        search_context = self.search_handler.get_search_context_data(
            self.context_object_name)
        context['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]
        context['news_banners'] = self.get_banner_list(group_order=0, banner_in_type='IBP', display_in_page='NEWS')
        context.update(search_context)
        return context

