from django.contrib import admin
from treebeard.admin import TreeAdmin
from oscar.core.loading import get_model

NewsCategory = get_model('news', 'NewsCategory')
News = get_model('news', 'News')


class NewsCategoryAdmin(TreeAdmin):
    pass


class NewsAdmin(admin.ModelAdmin):
    date_hierarchy = 'date_created'


class LegalDocumentAdmin(admin.ModelAdmin):
    date_hierarchy = 'date_created'


admin.site.register(NewsCategory, NewsCategoryAdmin)
admin.site.register(News, NewsAdmin)
