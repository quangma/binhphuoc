# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import pgettext_lazy
from django.utils.translation import ugettext_lazy as _

from apps.address.models import District
from apps.catalogue.models import Category


class EnterpriseSearchForm(forms.Form):
    name = forms.CharField(required=False,
                           label=pgettext_lazy(u"Partner's name", u"Name"))
    category = forms.ModelChoiceField(queryset=Category.objects.filter(is_active=True, depth=1),
                                      required=False,
                                      label=_(u"Ngành hàng"))
    district = forms.ModelChoiceField(queryset=District.objects.all(),
                                      required=False,
                                      label=_(u"Quận/Huyện"))
