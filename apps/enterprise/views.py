from django.contrib import messages
from django.core.paginator import InvalidPage
from django.http import HttpResponsePermanentRedirect, Http404
from django.shortcuts import redirect, get_object_or_404
from django.utils.http import urlquote
from django.views import generic
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from oscar.core.loading import get_model

from apps.catalogue.models import Category
from apps.enterprise.forms import EnterpriseSearchForm
from apps.partner.settings import COMPANY_TYPES
from .search_handlers import EnterpriseProductSearchHandler

if settings.THEMES:
    THEME = settings.THEMES[0]

Partner = get_model('partner', 'Partner')
Product = get_model('catalogue', 'product')


class EnterpriseListView(generic.ListView):
    model = Partner
    context_object_name = 'enterprises'
    form_class = EnterpriseSearchForm
    template_name = '%s/enterprise/enterprise_list.html' % THEME
    paginate_by = 20
    ordering = '?'
    num_products = 20

    def get_queryset(self):
        """
        @return:
        """
        qs = self.model._default_manager.all()
        self.form = self.form_class(self.request.GET)
        if not self.form.is_valid():
            return qs
        data = self.form.cleaned_data
        if data.get('name'):
            qs = qs.filter(name__icontains=data['name'])
        if data.get('district'):
            qs = qs.filter(addresses__district=data['district'])
        if data.get('ward'):
            qs = qs.filter(addresses__ward=data['ward'])
        if data.get('type'):
            qs = qs.filter(type=data['type'])
        if data.get('category'):
            category = data['category']
            sub_categories = category.get_descendants_and_self()
            qs = qs.filter(category__in=sub_categories)
        return qs

    def get_context_data(self, **kwargs):
        ctx = super(EnterpriseListView, self).get_context_data(**kwargs)
        ctx['form'] = self.form
        ctx['best_selling_product'] = Product.get_best_selling_product(
            cat_id=None, num_product=self.num_products)
        return ctx


class EnterpriseView(generic.TemplateView):
    """
    Browse products in a given enterprise
    """
    context_object_name = "products"
    template_name = '%s/enterprise/enterprise_detail.html' % THEME
    enforce_paths = True
    num_products = 20
    num_enterprises = 20

    def get(self, request, *args, **kwargs):
        # Fetch the category; return 404 or redirect as needed
        self.enterprise = self.get_enterprise()
        potential_redirect = self.redirect_if_necessary(
            request.path, self.enterprise)
        if potential_redirect is not None:
            return potential_redirect

        try:
            if 'pk' in self.kwargs:
                enterprise_id = self.kwargs['pk']
            else:
                enterprise_id = None
            self.search_handler = self.get_search_handler(
                request.GET, request.get_full_path(), enterprise_id)
        except InvalidPage:
            messages.error(request, _('The given page number was invalid.'))
            return redirect(self.enterprise.get_absolute_url())

        return super(EnterpriseView, self).get(request, *args, **kwargs)

    def get_enterprise(self):
        if 'pk' in self.kwargs:
            return get_object_or_404(Partner, pk=self.kwargs['pk'])
        raise Http404

    def redirect_if_necessary(self, current_path, enterprise):
        if self.enforce_paths:
            # Categories are fetched by primary key to allow slug changes.
            # If the slug has changed, issue a redirect.
            expected_path = enterprise.get_absolute_url()
            if expected_path != urlquote(current_path):
                return HttpResponsePermanentRedirect(expected_path)

    def get_search_handler(self, *args, **kwargs):
        return EnterpriseProductSearchHandler(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EnterpriseView, self).get_context_data(**kwargs)
        context['enterprise'] = self.enterprise
        search_context = self.search_handler.get_search_context_data(
            self.context_object_name)
        context.update(search_context)
        context['num_products'] = context['paginator'].count
        context['best_selling_product'] = Product.get_best_selling_product(
            cat_id=None, num_product=self.num_products)
        context['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]
        return context


class RandomEnterpriseListView(generic.TemplateView):
    template_name = '%s/enterprise/rand_enterprise_list.html' % THEME
    num_enterprises = 20
    num_products = 20

    def get_context_data(self, **kwargs):
        ctx = super(RandomEnterpriseListView, self).get_context_data(**kwargs)
        ctx['enterprises'] = Partner.objects.all().order_by('?')[:self.num_enterprises]
        ctx['best_selling_product'] = Product.get_best_selling_product(
            cat_id=None, num_product=self.num_products)
        return ctx
