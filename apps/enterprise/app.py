from django.conf.urls import url

from oscar.core.application import Application
from oscar.core.loading import get_class


class EnterpriseApplication(Application):
    name = 'enterprise'
    enterprise_list_views = get_class('enterprise.views', 'EnterpriseListView')
    enterprise_view = get_class('enterprise.views', 'EnterpriseView')
    random_enterprise_view = get_class('enterprise.views', 'RandomEnterpriseListView')

    def get_urls(self):
        urlpatterns = super(EnterpriseApplication, self).get_urls()
        urlpatterns += [
            url(r'^$', self.enterprise_list_views.as_view(), name='index'),
            url(r'^danh-sach-ngau-nhien', self.random_enterprise_view.as_view(), name='random-list'),
            url(r'^(?P<enterprise_slug>[\w-]+(/[\w-]+)*)_(?P<pk>\d+)/$',
                self.enterprise_view.as_view(), name='enterprise'),
        ]
        return self.post_process_urls(urlpatterns)

application = EnterpriseApplication()