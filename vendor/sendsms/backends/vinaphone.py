# -*- coding: utf-8 -*-
"""
VINA_PHONE sms gateway backend. (https://ads.vinaphone.com.vn/)
{
    "RQST": {
        "name": "Gui tin nhan cho Vant",
        "REQID": "100001",
        "LABELID": "51377",
        "CONTRACTTYPEID": "1",
        "CONTRACTID": "4736",
        "TEMPLATEID": "207216",
        "PARAMS": [
            {
                "NUM": "1",
                "CONTENT": "20009"
            }
        ],
        "SCHEDULETIME": "04/07/2017 10:25",
        "MOBILELIST": "84986931488",
        "ISTELCOSUB": "0",
        "AGENTID": "242",
        "APIUSER": "EcomBacNinh",
        "APIPASS": "123456@Ec",
        "USERNAME": "EcomBacNinh"
    }
}

Configuration example.
~~~~~~~~~~~~~~~~~~~~~~

Modify your settings.py::
    
    VINA_PHONE_USERNAME = 'EcomBacNinh'
    VINA_PHONE_PASSWORD = '123456@Ec'
    VINA_PHONE_ACCOUNT = 'EcomBacNinh'
    VINA_PHONE_SANDBOX = False # True if yo like test first
    INSTALLED_APPS += ['sendsms']

Usage::
    
    from sendsms.message import SmsMessage
    message = SmsMessage(
        body = 'my 160 chars sms',
        from_phone = '111111111',
        to = ['222222222']
    )
    message.send()

<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <sendsms_brandname xmlns="http://tempuri.org/">
      <modile>0986931488</modile>
      <noidung>Van kiem tra kha nang gui tin nhan cho Viettel</noidung>
      <s_acc>vantxm2010</s_acc>
      <s_pas>vantxm2010</s_pas>
    </sendsms_brandname>
  </soap:Body>
</soap:Envelope>


"""

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils.encoding import force_unicode

import requests

from .base import BaseSmsBackend

VINA_PHONE_API_URL = 'https://sms.ecombinhphuoc.vn/Service_sendSMS.asmx'


class SmsBackend(BaseSmsBackend):
    """ 
    SMS Backend for VINA_PHONE.es provider.

    The methods "get_xxxxxx" serve to facilitate the inheritance. Thus if a private 
    project in the access data are dynamic, and are stored in the database. A child 
    class overrides the method "get_xxxx" to return data stored in the database.
    """

    def _send(self, message):
        """
        Private method for send one message.

        :param SmsMessage message: SmsMessage class instance.
        :returns: True if message is sent else False
        :rtype: bool
        """
        headers = {'content-type': 'text/xml'}
        # todo: bỏ verify=False đi vì sẽ sinh lỗi
        response = requests.post(VINA_PHONE_API_URL, data=message.body, headers=headers, verify=False)
        if response.status_code != 200:
            if not self.fail_silently:
                raise Exception('Bad status code')
            else:
                return False
        
        if 'success' not in response.content:
            if not self.fail_silently:
                raise Exception('Bad result')
            else: 
                return False

        # response = self._parse_response(response.content)
        
        if 'success' in response.content:
            return True
        
        return False

    def send_messages(self, messages):
        """
        Send messages.

        :param list messages: List of SmsMessage instences.
        :returns: number of messages seded succesful.
        :rtype: int
        """
        counter = 0
        for message in messages:
            res = self._send(message)
            if res:
                counter += 1

        return counter
