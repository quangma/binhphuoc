# -*- coding: utf-8 -*-

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ContactFormConfig(AppConfig):
    label = 'contact_form'
    name = 'contact_form'
    verbose_name = _(u'Thông tin liên hệ')
