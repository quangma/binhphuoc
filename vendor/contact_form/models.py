# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Contact(models.Model):
    """
    """
    name = models.CharField(max_length=100,
                            verbose_name=_(u'Họ tên người liên hệ'))
    company_name = models.CharField(max_length=256, verbose_name=_(u'Đơn vị/Công ty'))
    address = models.CharField(max_length=512, verbose_name=_(u'Địa chỉ'))
    email = models.EmailField(verbose_name=_(u'Email'))
    tel = models.CharField(max_length=16, verbose_name=_(u"Số điện thoại"))
    fax = models.CharField(max_length=16, verbose_name=_(u"Fax"), blank=True)
    title = models.CharField(max_length=128, verbose_name=_(u"Tiêu đề"), blank=True)
    body = models.TextField(verbose_name=_(u'Nội dung liên hệ'))
    file = models.FileField(verbose_name=_(u"file đính kèm"), blank=True)
    date_created = models.DateTimeField(verbose_name=_(u"Ngày gửi"), auto_now=True, editable=False)

    class Meta:
        verbose_name = _(u"Thông tin liên hệ")
        verbose_name_plural = _(u"Thông tin liên hệ")
