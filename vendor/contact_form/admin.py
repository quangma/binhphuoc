# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Contact


class ContactAdmin(admin.ModelAdmin):
    model = Contact

    list_display = ['name', 'company_name', 'email', 'tel', 'date_created', 'file', ]
    ordering = ['-date_created']


admin.site.register(Contact, ContactAdmin)
