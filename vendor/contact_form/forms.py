# -*- coding: utf-8 -*-

"""
A base contact form for allowing users to send email messages through
a web interface.

"""

from django import forms
from django.contrib.sites.requests import RequestSite
from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.contrib.sites.models import Site
from django.template.defaultfilters import filesizeformat
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _

# from captcha.fields import CaptchaField
from .models import Contact
import settings


class ContactForm(forms.ModelForm):
    """
    The base contact form class from which all contact form classes
    should inherit.

    """
    # captcha = CaptchaField()

    from_email = settings.DEFAULT_FROM_EMAIL

    recipient_list = [mail_tuple[1] for mail_tuple in settings.MANAGERS]

    subject_template_name = "binhphuoc/contact_form/contact_form_subject.txt"

    template_name = 'binhphuoc/contact_form/contact_form.txt'

    class Meta:
        model = Contact
        fields = '__all__'

    def __init__(self, data=None, files=None, request=None, recipient_list=None,
                 *args, **kwargs):
        if request is None:
            raise TypeError("Keyword argument 'request' must be supplied")
        self.request = request
        if recipient_list is not None:
            self.recipient_list = recipient_list
        super(ContactForm, self).__init__(data=data, files=files, *args, **kwargs)

    def message(self):
        """
        Render the body of the message to a string.

        """
        if callable(self.template_name):
            template_name = self.template_name()
        else:
            template_name = self.template_name
        return loader.render_to_string(template_name,
                                       self.get_context())

    def subject(self):
        """
        Render the subject of the message to a string.

        """
        subject = loader.render_to_string(self.subject_template_name,
                                          self.get_context())
        return ''.join(subject.splitlines())

    def get_context(self):
        """
        Return the context used to render the templates for the email
        subject and body.

        By default, this context includes:

        * All of the validated values in the form, as variables of the
          same names as their fields.

        * The current ``Site`` object, as the variable ``site``.

        * Any additional variables added by context processors (this
          will be a ``RequestContext``).

        """
        if not self.is_valid():
            raise ValueError("Cannot generate Context from invalid contact form")
        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(self.request)
        return dict(self.cleaned_data, site=site)

    def get_message_dict(self):
        """
        Generate the various parts of the message and return them in a
        dictionary, suitable for passing directly as keyword arguments
        to ``django.core.mail.send_mail()``.

        By default, the following values are returned:

        * ``from_email``

        * ``message``

        * ``recipient_list``

        * ``subject``

        """
        if not self.is_valid():
            raise ValueError("Message cannot be sent from invalid contact form")
        message_dict = {}
        for message_part in ('from_email', 'subject'):
            attr = getattr(self, message_part)
            message_dict[message_part] = attr() if callable(attr) else attr
        message_dict['to'] = self.recipient_list
        message_dict['body'] = self.cleaned_data['body']
        return message_dict

    def clean_file(self):
        attach_file = self.cleaned_data['file']
        import os
        if attach_file:
            # file_type = attach_file.content_type.split('/')[0]
            ext = os.path.splitext(attach_file.name)[1]  # [0] returns path+filename
            print(ext)
            print(attach_file._size)
            if not ext.lower() in settings.CONTACT_UPLOAD_FILE_TYPES:
                raise forms.ValidationError(_(u'Không hỗ trợ kiểu file này'))
            else:
                if attach_file._size > int(settings.CONTACT_UPLOAD_FILE_MAX_SIZE):
                    raise forms.ValidationError(
                        _(u'Vui lòng upload file với kích thước nhỏ hơn %s. Kích thước file hiện tại là %s')
                        % (
                            filesizeformat(settings.CONTACT_UPLOAD_FILE_MAX_SIZE), filesizeformat(attach_file._size)))
        return attach_file

    def save(self, fail_silently=False):
        """
        # Build and send the email message.

        """
        # send_mail(fail_silently=fail_silently, **self.get_message_dict())
        attach_file = self.cleaned_data['file']
        name = self.cleaned_data['name']
        subject = self.subject()
        email = self.cleaned_data['email']
        company_name = self.cleaned_data['company_name']
        tel = self.cleaned_data['tel']
        body = self.cleaned_data['body']
        msg_plain = render_to_string('binhphuoc/contact_form/email.txt', {'name': name,
                                                                       'email': email,
                                                                       'company_name': company_name,
                                                                       'tel': tel,
                                                                       'body': body})
        msg_html = render_to_string('binhphuoc/contact_form/email.html', {'name': name,
                                                                       'email': email,
                                                                       'company_name': company_name,
                                                                       'tel': tel,
                                                                       'body': body})
        mail = EmailMultiAlternatives(subject=subject, body=msg_plain, from_email=email, to=self.recipient_list)
        mail.attach_alternative(msg_html, "text/html")
        if attach_file:
            mail.attach(attach_file.name, attach_file.read(), attach_file.content_type)
        mail.send(fail_silently=fail_silently)
        return super(ContactForm, self).save()
